package dependencies

import (
	"testing"

	"gitlab.com/SkynetLabs/skyd/skymodules"
)

// newTestDependencyWithDisableAndEnable returns an enable-disable dependency
// used for testing
func newTestDependencyWithDisableAndEnable(str string) *DependencyWithDisableAndEnable {
	return newDependencywithDisableAndEnable(str)
}

// testDependency is a basic dependency used for testing
type testDependency struct {
	skymodules.SkynetDependencies
}

// Disrupt returns true if the correct string is provided.
func (d *testDependency) Disrupt(s string) bool {
	return s == "TestDependency"
}

// TestMultiDependency is a small unit test that covers the functionality of the
// MultiDependency
func TestMultiDependency(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	t.Parallel()

	dep1 := newTestDependencyWithDisableAndEnable("dep1")
	dep2 := newTestDependencyWithDisableAndEnable("dep2")
	dep3 := &testDependency{}
	deps := NewMultiDependency(dep1, dep2, dep3)

	// assert default disrupt behaviour
	if !deps.Disrupt("dep1") || !deps.Disrupt("dep2") || !deps.Disrupt("TestDependency") || deps.Disrupt("Random") {
		t.Fatal("unexpected")
	}

	// disable the first dependency and assert it disrupts as expected
	dep1.Disable()
	if deps.Disrupt("dep1") || !deps.Disrupt("dep2") || !deps.Disrupt("TestDependency") {
		t.Fatal("unexpected")
	}

	// disable the second dependency and assert it disrupts as expected
	dep2.Disable()
	if deps.Disrupt("dep1") || deps.Disrupt("dep2") || !deps.Disrupt("TestDependency") {
		t.Fatal("unexpected")
	}
}
