package skymodules

import (
	"fmt"

	"gitlab.com/SkynetLabs/skyd/build"
	"go.sia.tech/siad/modules"
)

// AlertIDWorkerAccountBalanceDrift is a function that returns the ID of the
// alert that is registered when a worker's account balance has drifted from
// what the host claims to be in the renter's ephemeral account, more than the
// allowed threshold
//
// NOTE: the ID is unique to a worker as we supply a host's pubkey
func AlertIDWorkerAccountBalanceDrift(workerHostKey string) modules.AlertID {
	return modules.AlertID(fmt.Sprintf("balance-drift-%v", workerHostKey))
}

// AlertIDWorkerAccountOutOfFunds is a function that returns the ID of the alert
// that is registered when a worker's encountering insufficient balance errors
// from the host, while according to its own accounting the balance has not
// dropped below the refill threshold
//
// NOTE: the ID is unique to a worker as we supply a host's pubkey
func AlertIDWorkerAccountOutOfFunds(workerHostKey string) modules.AlertID {
	return modules.AlertID(fmt.Sprintf("out-of-funds-%v", workerHostKey))
}

// NewAlerter creates a new alerter for a given module. When building for
// testing this will also register some dummy alerts.
func NewAlerter(module string) *modules.GenericAlerter {
	a := modules.NewAlerter(module)

	// In testing we register some alerts.
	if build.Release != "testing" {
		return a
	}
	a.RegisterAlert(modules.AlertID(module+" - Dummy0"), "msg0", "cause0", modules.SeverityInfo)
	a.RegisterAlert(modules.AlertID(module+" - Dummy1"), "msg1", "cause1", modules.SeverityWarning)
	a.RegisterAlert(modules.AlertID(module+" - Dummy2"), "msg2", "cause2", modules.SeverityError)
	a.RegisterAlert(modules.AlertID(module+" - Dummy3"), "msg3", "cause3", modules.SeverityCritical)
	return a
}
