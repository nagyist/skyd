package proto

import (
	"io"

	"gitlab.com/NebulousLabs/errors"

	"gitlab.com/NebulousLabs/encoding"
	"gitlab.com/SkynetLabs/skyd/skymodules"
	"go.sia.tech/siad/crypto"
	"go.sia.tech/siad/types"
)

// v132UpdateHeader was introduced due to backwards compatibility reasons after
// changing the format of the contractHeader. It contains the legacy
// v132ContractHeader.
type v132UpdateSetHeader struct {
	ID     types.FileContractID
	Header v132ContractHeader
}

// v160UpdateHeader was introduced due to backwards compatibility reasons after
// changing the format of the contractHeader. It contains the legacy
// v1420ContractHeader.
type v160UpdateSetHeader struct {
	ID     types.FileContractID
	Header v1420ContractHeader
}

// v132ContractHeader is a contractHeader without the Utility field. This field
// was added after v132 to be able to persist contract utilities.
type v132ContractHeader struct {
	// transaction is the signed transaction containing the most recent
	// revision of the file contract.
	Transaction types.Transaction

	// secretKey is the key used by the renter to sign the file contract
	// transaction.
	SecretKey crypto.SecretKey

	// Same as skymodules.RenterContract.
	StartHeight      types.BlockHeight
	DownloadSpending types.Currency
	StorageSpending  types.Currency
	UploadSpending   types.Currency
	TotalCost        types.Currency
	ContractFee      types.Currency
	TxnFee           types.Currency
	SiafundFee       types.Currency
}

// v1412ContractHeader is the contract header that was used up to and including
// v1.4.1.2
type v1412ContractHeader struct {
	// transaction is the signed transaction containing the most recent
	// revision of the file contract.
	Transaction types.Transaction

	// secretKey is the key used by the renter to sign the file contract
	// transaction.
	SecretKey crypto.SecretKey

	// Same as skymodules.RenterContract.
	StartHeight      types.BlockHeight
	DownloadSpending types.Currency
	StorageSpending  types.Currency
	UploadSpending   types.Currency
	TotalCost        types.Currency
	ContractFee      types.Currency
	TxnFee           types.Currency
	SiafundFee       types.Currency

	GoodForUpload bool
	GoodForRenew  bool
	LastOOSErr    types.BlockHeight
	Locked        bool
}

// v1420ContractUtility is the contract utility that was used up to and including
// v1.6.0.
type v1420ContractUtility struct {
	GoodForUpload bool `json:"goodforupload"`
	GoodForRenew  bool `json:"goodforrenew"`

	// BadContract will be set to true if there's good reason to believe that
	// the contract is unusable and will continue to be unusable. For example,
	// if the host is claiming that the contract does not exist, the contract
	// should be marked as bad.
	BadContract bool              `json:"badcontract"`
	LastOOSErr  types.BlockHeight `json:"lastooserr"` // OOS means Out Of Storage

	// If a contract is locked, the utility should not be updated. 'Locked' is a
	// value that gets persisted.
	Locked bool `json:"locked"`
}

// v1420ContractHeader is the contract header that was used up to and including
// v1.4.2.
type v1420ContractHeader struct {
	// transaction is the signed transaction containing the most recent
	// revision of the file contract.
	Transaction types.Transaction

	// secretKey is the key used by the renter to sign the file contract
	// transaction.
	SecretKey crypto.SecretKey

	// Same as skymodules.RenterContract.
	StartHeight         types.BlockHeight
	DownloadSpending    types.Currency
	FundAccountSpending types.Currency
	MaintenanceSpending skymodules.MaintenanceSpending
	StorageSpending     types.Currency
	UploadSpending      types.Currency
	TotalCost           types.Currency
	ContractFee         types.Currency
	TxnFee              types.Currency
	SiafundFee          types.Currency
	Utility             v1420ContractUtility
}

// contractHeaderDecodeV1412ToV160 attempts to decode a contract header using
// the persist struct as of v1.4.1.2, returning a header that has been converted
// to the v1.6.0 version of the header.
func contractHeaderDecodeV1412ToV160(f io.ReadSeeker, decodeMaxSize int) (contractHeader, error) {
	_, err := f.Seek(0, 0)
	if err != nil {
		return contractHeader{}, errors.AddContext(err, "unable to reset file when attempting legacy decode")
	}
	var v1412Header v1412ContractHeader
	err = encoding.NewDecoder(f, decodeMaxSize).Decode(&v1412Header)
	if err != nil {
		return contractHeader{}, errors.AddContext(err, "unable to decode header as a v1412 header")
	}

	return contractHeader{
		Transaction: v1412Header.Transaction,

		SecretKey: v1412Header.SecretKey,

		StartHeight:      v1412Header.StartHeight,
		DownloadSpending: v1412Header.DownloadSpending,
		StorageSpending:  v1412Header.StorageSpending,
		UploadSpending:   v1412Header.UploadSpending,
		TotalCost:        v1412Header.TotalCost,
		ContractFee:      v1412Header.ContractFee,
		TxnFee:           v1412Header.TxnFee,
		SiafundFee:       v1412Header.SiafundFee,

		Utility: skymodules.ContractUtility{
			GoodForUpload: v1412Header.GoodForUpload,
			GoodForRenew:  v1412Header.GoodForRenew,
			BadContract:   false,
			LastOOSErr:    v1412Header.LastOOSErr,
			Locked:        v1412Header.Locked,
		},
	}, nil
}

// contractHeaderDecodeV1412ToV1420 attempts to decode a contract header using
// the persist struct as of v1.4.1.2, returning a header that has been converted
// to the v1.4.2 version of the header.
func contractHeaderDecodeV1420ToV160(f io.ReadSeeker, decodeMaxSize int) (contractHeader, error) {
	_, err := f.Seek(0, 0)
	if err != nil {
		return contractHeader{}, errors.AddContext(err, "unable to reset file when attempting legacy decode")
	}
	var v160Header v1420ContractHeader
	err = encoding.NewDecoder(f, decodeMaxSize).Decode(&v160Header)
	if err != nil {
		return contractHeader{}, errors.AddContext(err, "unable to decode header as a v1412 header")
	}

	return contractHeader{
		Transaction: v160Header.Transaction,

		SecretKey: v160Header.SecretKey,

		StartHeight:      v160Header.StartHeight,
		DownloadSpending: v160Header.DownloadSpending,
		StorageSpending:  v160Header.StorageSpending,
		UploadSpending:   v160Header.UploadSpending,
		TotalCost:        v160Header.TotalCost,
		ContractFee:      v160Header.ContractFee,
		TxnFee:           v160Header.TxnFee,
		SiafundFee:       v160Header.SiafundFee,

		Utility: skymodules.ContractUtility{
			GoodForUpload:  v160Header.Utility.GoodForUpload,
			GoodForRefresh: v160Header.Utility.GoodForRenew, // set it to the same as GoodForRenew
			GoodForRenew:   v160Header.Utility.GoodForRenew,
			BadContract:    v160Header.Utility.BadContract,
			LastOOSErr:     v160Header.Utility.LastOOSErr,
			Locked:         v160Header.Utility.Locked,
		},
	}, nil
}

// updateSetHeaderUnmarshalV132ToV160 attempts to unmarshal an update set header
// using the v1.3.2 encoding scheme, returning a v1.6.0 version of the update
// set header.
func updateSetHeaderUnmarshalV132ToV160(b []byte, u *updateSetHeader) error {
	var oldHeader v132UpdateSetHeader
	if err := encoding.Unmarshal(b, &oldHeader); err != nil {
		// If unmarshaling the header the old way also doesn't work we
		// return the original error.
		return errors.AddContext(err, "could not unmarshal update into v1.3.2 format")
	}
	// If unmarshaling it the old way was successful we convert it to a new
	// header.
	u.Header = contractHeader{
		Transaction:      oldHeader.Header.Transaction,
		SecretKey:        oldHeader.Header.SecretKey,
		StartHeight:      oldHeader.Header.StartHeight,
		DownloadSpending: oldHeader.Header.DownloadSpending,
		StorageSpending:  oldHeader.Header.StorageSpending,
		UploadSpending:   oldHeader.Header.UploadSpending,
		TotalCost:        oldHeader.Header.TotalCost,
		ContractFee:      oldHeader.Header.ContractFee,
		TxnFee:           oldHeader.Header.TxnFee,
		SiafundFee:       oldHeader.Header.SiafundFee,
	}
	return nil
}

// updateSetHeaderUnmarshalV1420ToV160 attempts to unmarshal an update set
// header using the v1.3.2 encoding scheme, returning a v1.4.2 version of the
// update set header.
func updateSetHeaderUnmarshalV1420ToV160(b []byte, u *updateSetHeader) error {
	var oldHeader v160UpdateSetHeader
	if err := encoding.Unmarshal(b, &oldHeader); err != nil {
		// If unmarshaling the header the old way also doesn't work we
		// return the original error.
		return errors.AddContext(err, "could not unmarshal update into v1.6.0 format")
	}
	// If unmarshaling it the old way was successful we convert it to a new
	// header.
	u.Header = contractHeader{
		Transaction:         oldHeader.Header.Transaction,
		SecretKey:           oldHeader.Header.SecretKey,
		StartHeight:         oldHeader.Header.StartHeight,
		DownloadSpending:    oldHeader.Header.DownloadSpending,
		FundAccountSpending: oldHeader.Header.FundAccountSpending,
		MaintenanceSpending: oldHeader.Header.MaintenanceSpending,
		StorageSpending:     oldHeader.Header.StorageSpending,
		UploadSpending:      oldHeader.Header.UploadSpending,
		TotalCost:           oldHeader.Header.TotalCost,
		ContractFee:         oldHeader.Header.ContractFee,
		TxnFee:              oldHeader.Header.TxnFee,
		SiafundFee:          oldHeader.Header.SiafundFee,
		Utility: skymodules.ContractUtility{
			GoodForUpload:  oldHeader.Header.Utility.GoodForUpload,
			GoodForRefresh: oldHeader.Header.Utility.GoodForRenew, // set it to the same as GoodForRenew
			GoodForRenew:   oldHeader.Header.Utility.GoodForRenew,
			BadContract:    oldHeader.Header.Utility.BadContract,
			LastOOSErr:     oldHeader.Header.Utility.LastOOSErr,
			Locked:         oldHeader.Header.Utility.Locked,
		},
	}
	return nil
}
