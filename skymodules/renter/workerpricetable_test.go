package renter

import (
	"bytes"
	"fmt"
	"strings"
	"sync/atomic"
	"testing"
	"time"

	"gitlab.com/NebulousLabs/errors"
	"gitlab.com/NebulousLabs/fastrand"
	"gitlab.com/SkynetLabs/skyd/build"
	"gitlab.com/SkynetLabs/skyd/siatest/dependencies"
	"gitlab.com/SkynetLabs/skyd/skymodules"
	"go.sia.tech/siad/crypto"
	"go.sia.tech/siad/modules"
	"go.sia.tech/siad/types"
)

// TestUpdatePriceTableHostHeightLeeway verifies the worker will verify the
// HostHeight when updating the price table and will not accept a height that is
// significantly lower than our blockheight.
func TestUpdatePriceTableHostHeightLeeway(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	t.Parallel()

	// create a new worker tester
	wt, err := newWorkerTester(t.Name())
	if err != nil {
		t.Fatal(err)
	}
	defer func() {
		err := wt.Close()
		if err != nil {
			t.Fatal(err)
		}
	}()
	w := wt.worker

	// prevent cache updates.
	atomic.StoreUint64(&w.atomicCacheUpdating, 1)

	// verify recent err is nil
	if w.staticPriceTable().staticRecentErr != nil {
		t.Fatal("unexpected")
	}

	// corrupt the synced property on the renter.
	w.staticRenter.csMu.Lock()
	w.staticRenter.csSynced = false
	w.staticRenter.csBlockHeight *= 2
	w.staticRenter.csMu.Unlock()

	// corrupt the price table's update time so we are allowed to update
	wpt := w.staticPriceTable()
	wptc := new(workerPriceTable)
	wptc.staticExpiryTime = wpt.staticExpiryTime
	wptc.staticUpdateTime = time.Now().Add(-time.Second)
	wptc.staticPriceTable = wpt.staticPriceTable
	w.staticSetPriceTable(wptc)

	// verify the update errored out and rejected the host's price table due to
	// an invalid blockheight
	err = build.Retry(600, 100*time.Millisecond, func() error {
		w.staticWake()
		err = w.staticPriceTable().staticRecentErr
		if !errors.Contains(err, errHostBlockHeightNotWithinTolerance) {
			return errors.New("pricetable was not rejected")
		}
		return nil
	})
	if err != nil {
		t.Fatal(err)
	}
}

// TestUpdatePriceTableGouging checks that the price table gouging is correctly
// detecting price gouging from a host.
func TestUpdatePriceTableGouging(t *testing.T) {
	t.Parallel()

	// allowance contains only the fields necessary to test the price gouging
	allowance := skymodules.Allowance{
		Funds:  types.SiacoinPrecision,
		Period: types.BlockHeight(6),
	}

	// verify happy case
	pt := newDefaultPriceTable()
	err := checkUpdatePriceTableGouging(pt, allowance)
	if err != nil {
		t.Fatal("unexpected price gouging failure")
	}

	// verify gouging case, first calculate how many times we need to update the
	// PT over the duration of the allowance period
	pt = newDefaultPriceTable()
	durationInS := int64(pt.Validity.Seconds())
	periodInS := int64(allowance.Period) * 10 * 60 // period times 10m blocks
	numUpdates := periodInS / durationInS

	// increase the update price table cost so that the total cost of updating
	// it for the entire allowance period exceeds the allowed percentage of the
	// total allowance.
	pt.UpdatePriceTableCost = allowance.Funds.MulFloat(updatePriceTableGougingPercentageThreshold * 2).Div64(uint64(numUpdates))
	err = checkUpdatePriceTableGouging(pt, allowance)
	if err == nil || !strings.Contains(err.Error(), "update price table cost") {
		t.Fatalf("expected update price table cost gouging error, instead error was '%v'", err)
	}

	// verify unacceptable validity case
	pt = newDefaultPriceTable()
	pt.Validity = 0
	err = checkUpdatePriceTableGouging(pt, allowance)
	if err == nil || !strings.Contains(err.Error(), "update price table validity") {
		t.Fatalf("expected update price table validity gouging error, instead error was '%v'", err)
	}
	pt.Validity = minAcceptedPriceTableValidity
	err = checkUpdatePriceTableGouging(pt, allowance)
	if err != nil {
		t.Fatalf("unexpected update price table validity gouging error: %v", err)
	}
}

// TestUpdatePriceTablePayment verifies the price tables updates are paid for
// using the EA if possible
func TestUpdatePriceTablePayment(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	t.Parallel()

	// create a new worker tester with a disabled worker
	wt, err := newWorkerTesterCustomDependency(t.Name(), &dependencies.DependencyDisableWorker{}, modules.ProdDependencies)
	if err != nil {
		t.Fatal(err)
	}
	defer func() {
		err := wt.Close()
		if err != nil {
			t.Fatal(err)
		}
	}()

	// fetch the maintenance spending before doing anything
	financials, err := wt.rt.renter.PeriodSpending()
	if err != nil {
		t.Fatal(err)
	}

	// assert we've spent 0 money from our contract on updating price tables
	if !financials.MaintenanceSpending.UpdatePriceTableCost.IsZero() {
		t.Fatal("unexpected financials", financials.MaintenanceSpending.UpdatePriceTableCost)
	}

	// fetch a price table and refill the account manually
	wt.staticUpdatePriceTable()
	wt.managedRefillAccount()

	// fetch the update PT cost
	pt := wt.worker.staticPriceTable()
	updatePTCost := pt.staticPriceTable.UpdatePriceTableCost
	balanceTarget := wt.worker.staticRenter.staticAccountBalanceTarget

	// assert the worker's maintenance is OK
	wt.worker.staticMaintenanceState.accountSyncSucceeded = true
	wt.worker.staticMaintenanceState.revisionsMismatchFixSucceeded = true
	if !wt.worker.managedMaintenanceSucceeded() {
		t.Fatal("unexpected maintenance status")
	}

	// assert the update was paid for by contract
	financials, err = wt.rt.renter.PeriodSpending()
	if err != nil {
		t.Fatal(err)
	}
	if !financials.MaintenanceSpending.UpdatePriceTableCost.Equals(updatePTCost) {
		t.Fatal("unexpected financials", financials.MaintenanceSpending.UpdatePriceTableCost)
	}

	// fetch the current EA balance and assert it's full
	balance := wt.worker.staticAccount.managedAvailableBalance()
	if !balance.Equals(balanceTarget) {
		t.Fatal("unexpected balance", balance)
	}

	// update it again and assert it renewed
	pt.staticUpdateTime = time.Now().Add(-time.Minute)
	wt.staticUpdatePriceTable()
	if wt.worker.staticPriceTable().staticPriceTable.UID == pt.staticPriceTable.UID {
		t.Fatal("expected the price table to be renewed", pt.staticPriceTable.UID)
	}

	// assert the update was paid for by EA
	if !wt.worker.staticAccount.managedAvailableBalance().Add(updatePTCost).Equals(balanceTarget) {
		t.Fatal("unexpected account balance")
	}

	// mock a worker's maintenance failure
	wt.worker.staticMaintenanceState.priceTableUpdateSucceeded = false

	// update it again and assert it renewed
	wt.worker.staticPriceTable().staticUpdateTime = time.Now().Add(-time.Minute)
	wt.staticUpdatePriceTable()

	// assert the update was paid for by contract
	financials, err = wt.rt.renter.PeriodSpending()
	if err != nil {
		t.Fatal(err)
	}
	if !financials.MaintenanceSpending.UpdatePriceTableCost.Equals(updatePTCost.Mul64(2)) {
		t.Fatal("unexpected financials", financials.MaintenanceSpending.UpdatePriceTableCost)
	}

	// assert the maintenance state is reset after the successful refill
	if !wt.worker.managedMaintenanceSucceeded() {
		t.Fatal("unexpected maintenance status")
	}

	// update the pricetable
	wt.worker.staticPriceTable().staticUpdateTime = time.Now().Add(-time.Minute)
	wt.staticUpdatePriceTable()

	// assert the update was paid for by EA
	if !wt.worker.staticAccount.managedAvailableBalance().Add(updatePTCost.Mul64(2)).Equals(balanceTarget) {
		t.Fatal("unexpected account balance")
	}

	// set the account balance to below the pay by ea threshold
	threshold := balanceTarget.MulFloat(maintenancePayByEAPercentageThreshold)
	wt.worker.staticAccount.balance = threshold.Sub64(1)

	// update the pricetable
	wt.worker.staticPriceTable().staticUpdateTime = time.Now().Add(-time.Minute)
	wt.staticUpdatePriceTable()

	// assert the update was paid for by contract
	financials, err = wt.rt.renter.PeriodSpending()
	if err != nil {
		t.Fatal(err)
	}
	if !financials.MaintenanceSpending.UpdatePriceTableCost.Equals(updatePTCost.Mul64(3)) {
		t.Fatal("unexpected financials", financials.MaintenanceSpending.UpdatePriceTableCost)
	}
}

// TestSchedulePriceTableUpdate verifies whether scheduling a price table update
// on the worker effectively executes a price table update immediately after it
// being scheduled.
func TestSchedulePriceTableUpdate(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	t.Parallel()

	// create a dependency that makes the host refuse a price table
	deps := dependencies.NewDependencyHostLosePriceTable()
	deps.Disable()

	// create a new worker tester
	wt, err := newWorkerTesterCustomDependency(t.Name(), skymodules.SkydProdDependencies, deps)
	if err != nil {
		t.Fatal(err)
	}
	defer func() {
		err := wt.Close()
		if err != nil {
			t.Fatal(err)
		}
	}()
	w := wt.worker

	// keep track of the current values
	pt := w.staticPriceTable()
	cUID := pt.staticPriceTable.UID
	cUpdTime := pt.staticUpdateTime

	// schedule an update
	w.staticTryForcePriceTableUpdate()

	// check whether the price table got updated in a retry
	err = build.Retry(100, 100*time.Millisecond, func() error {
		if time.Now().After(cUpdTime) {
			t.Fatal("the price table updated according to the original schedule")
		}
		pt := w.staticPriceTable()
		if bytes.Equal(pt.staticPriceTable.UID[:], cUID[:]) {
			return errors.New("not updated yet")
		}
		return nil
	})
	if err != nil {
		t.Fatal(err)
	}

	// we have to manually update the pricetable here and reset the time of the
	// last forced update to null to ensure we're allowed to schedule two
	// updates in short succession
	update := *w.staticPriceTable()
	update.staticLastForcedUpdate = time.Time{}
	w.staticSetPriceTable(&update)

	// keep track of the current values
	pt = w.staticPriceTable()
	cUID = pt.staticPriceTable.UID
	cUpdTime = pt.staticUpdateTime

	// enable the dependency
	deps.Enable()

	// create a dummy program
	pb := modules.NewProgramBuilder(&pt.staticPriceTable, 0)
	pb.AddHasSectorInstruction(crypto.Hash{})
	p, data := pb.Program()
	cost, _, _ := pb.Cost(true)
	jhs := new(jobHasSector)
	jhs.staticSectors = []crypto.Hash{{1, 2, 3}}
	ulBandwidth, dlBandwidth := jhs.callExpectedBandwidth()
	bandwidthCost, bandwidthRefund := mdmBandwidthCost(pt.staticPriceTable, ulBandwidth, dlBandwidth)
	cost = cost.Add(bandwidthCost)

	// execute it
	_, _, err = w.managedExecuteProgram(p, data, types.FileContractID{}, categoryDownload, cost, bandwidthRefund)
	w.staticHandleError(err)
	if !modules.IsPriceTableInvalidErr(err) {
		t.Fatal("unexpected")
	}

	// check whether the price table got updated in a retry, the error thrown
	// should have scheduled that
	err = build.Retry(100, 100*time.Millisecond, func() error {
		if time.Now().After(cUpdTime) {
			t.Fatal("the price table updated according to the original schedule")
		}
		pt := w.staticPriceTable()
		if bytes.Equal(pt.staticPriceTable.UID[:], cUID[:]) {
			return errors.New("not updated yet")
		}
		return nil
	})
	if err != nil {
		t.Fatal(err)
	}

	// now disable the dependency
	deps.Disable()

	// execute the same program
	_, _, err = w.managedExecuteProgram(p, data, types.FileContractID{}, categoryDownload, cost, bandwidthRefund)
	if err != nil {
		t.Fatal("unexpected")
	}

	// keep track of the current values
	pt = w.staticPriceTable()
	cUID = pt.staticPriceTable.UID
	cUpdTime = pt.staticUpdateTime

	// check whether the price table got updated in a retry, which should
	// **not** have been the case because not enough time elapsed since the last
	// manually scheduled update
	err = build.Retry(10, 50*time.Millisecond, func() error {
		pt := w.staticPriceTable()
		if !bytes.Equal(pt.staticPriceTable.UID[:], cUID[:]) {
			return errors.New("should not have been scheduled to update")
		}
		return nil
	})
	if err != nil {
		t.Fatal(err)
	}
}

// TestRegisterPriceTableHostDB ensures the worker updates the most recent price
// table of the host in the host db.
func TestRegisterPriceTableHostDB(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	t.Parallel()

	// create a dependency that prevents the worker from updating the price table in the host db
	deps := dependencies.NewDependencyNoRegisterPriceTable()
	deps.Disable()

	// create a new worker tester
	wt, err := newWorkerTesterCustomDependency(t.Name(), deps, skymodules.SkydProdDependencies)
	if err != nil {
		t.Fatal(err)
	}
	defer func() {
		err := wt.Close()
		if err != nil {
			t.Fatal(err)
		}
	}()

	// define some convenience variables
	w := wt.worker
	r := wt.rt.renter
	m := wt.rt.miner
	hdb := r.staticHostDB

	// Set an allowance.
	allowance := skymodules.DefaultAllowance
	allowance.MaxSectorAccessPrice = types.SiacoinPrecision.Div64(2)
	allowance.MaxUploadBandwidthPrice = types.SiacoinPrecision.Div64(2)
	allowance.MaxDownloadBandwidthPrice = types.SiacoinPrecision.Div64(2)
	err = r.staticHostContractor.SetAllowance(allowance)
	if err != nil {
		t.Fatal(err)
	}

	// we add two more hosts so we can check for the various types of gouging individually
	h2, err := wt.rt.addHost("host2")
	if err != nil {
		t.Fatal(err)
	}
	h3, err := wt.rt.addHost("host3")
	if err != nil {
		t.Fatal(err)
	}

	// wait for them to show up as workers.
	err = build.Retry(600, 100*time.Millisecond, func() error {
		_, err := wt.rt.miner.AddBlock()
		if err != nil {
			return err
		}
		r.staticWorkerPool.callUpdate(r)
		workers := r.staticWorkerPool.callWorkers()
		if len(workers) < 3 {
			return fmt.Errorf("expected %v workers but got %v", 3, len(workers))
		}
		return nil
	})
	if err != nil {
		t.Fatal(err)
	}

	// grab the host keys
	hpk1 := wt.host.PublicKey()
	hpk2 := h2.PublicKey()
	hpk3 := h3.PublicKey()
	hosts := []types.SiaPublicKey{hpk1, hpk2, hpk3}

	// keep track of the current values
	wpt := w.staticPriceTable()
	currUID := wpt.staticPriceTable.UID
	currUpdateTime := wpt.staticUpdateTime

	// assert the host db holds the worker's most recent price table
	pt := hdb.PriceTable(hpk1)
	if pt == nil || pt.UID.String() != currUID.String() {
		t.Fatalf("unexpected price table %+v", pt)
	}

	// schedule an update
	wt.worker.staticTryForcePriceTableUpdate()

	// check whether the price table got updated in a retry
	err = build.Retry(100, 100*time.Millisecond, func() error {
		if time.Now().After(currUpdateTime) {
			t.Fatal("the price table updated according to the original schedule")
		}
		uuid := w.staticPriceTable().staticPriceTable.UID
		if uuid.String() == currUID.String() {
			return errors.New("not updated yet")
		}
		return nil
	})
	if err != nil {
		t.Fatal(err)
	}

	// assert the host db holds the most recent price table
	pt = hdb.PriceTable(hpk1)
	if pt == nil || pt.UID.String() == currUID.String() {
		t.Fatalf("unexpected price table %+v", pt)
	}

	// check the contracts are marked as GFU
	for _, hpk := range hosts {
		utility, exists := r.ContractUtility(hpk)
		if !exists {
			t.Fatal("unexpected")
		}
		if !utility.GoodForUpload {
			t.Fatal("unexpected")
		}
	}

	// enable a dependency, preventing the price table to be overwritten and manually set
	// a price table for every host, each price table is price gouging in its own way,
	// sector access, download and upload
	deps.Enable()
	gouging1 := *pt
	gouging1.WriteBaseCost = types.SiacoinPrecision
	hdb.RegisterPriceTable(hpk1, gouging1)
	gouging2 := *pt
	fastrand.Read(gouging2.UID[:])
	gouging2.DownloadBandwidthCost = types.SiacoinPrecision
	hdb.RegisterPriceTable(hpk2, gouging2)
	gouging3 := *pt
	fastrand.Read(gouging3.UID[:])
	gouging3.UploadBandwidthCost = types.SiacoinPrecision
	hdb.RegisterPriceTable(hpk3, gouging3)

	// now mine blocks and check whether all 3 contracts are marked as !GFU due to gouging
	err = build.Retry(100, 100*time.Millisecond, func() error {
		_, err := m.AddBlock()
		if err != nil {
			return err
		}
		for _, hpk := range hosts {
			utility, exists := r.ContractUtility(hpk)
			if !exists {
				return errors.New("contract not found")
			}
			if utility.GoodForUpload {
				return errors.New("contract not marked !GFU yet")
			}
		}
		return nil
	})
	if err != nil {
		t.Fatal(err)
	}
}

// TestHostBlockHeightWithinTolerance is a unit test that covers the logic
// contained within the hostBlockHeightWithinTolerance helper.
func TestHostBlockHeightWithinTolerance(t *testing.T) {
	t.Parallel()

	l := priceTableHostBlockHeightLeeWay
	inputs := []struct {
		RenterSynced      bool
		RenterBlockHeight types.BlockHeight
		HostBlockHeight   types.BlockHeight
		ExpectedOutcome   bool
	}{
		{true, l - 1, 0, true},  // renter synced and hbh lower (verify underflow)
		{true, l, 0, true},      // renter synced and hbh lower
		{true, l + 1, 0, false}, // renter synced and hbh too low
		{false, l, 0, false},    // renter not synced and hbh lower
		{true, 1, 1 + l, true},  // renter synced and hbh higher
		{true, l + 1, 1, true},  // renter synced and hbh lower
		{true, 0, l + 1, false}, // renter synced and hbh too high
		{true, l + 2, 1, false}, // renter synced and hbh too low
		{false, 5, 4, false},    // renter not synced and hbh too low
		{false, 5, 5, true},     // renter not synced and hbh equal
		{false, 5, 6, true},     // renter not synced and hbh higher
	}

	for _, input := range inputs {
		if hostBlockHeightWithinTolerance(input.RenterSynced, input.RenterBlockHeight, input.HostBlockHeight) != input.ExpectedOutcome {
			t.Fatal("unexpected outcome", input)
		}
	}
}

// newDefaultPriceTable is a helper function that returns a price table with
// default prices for all fields
func newDefaultPriceTable() modules.RPCPriceTable {
	hes := modules.DefaultHostExternalSettings()
	oneCurrency := types.NewCurrency64(1)
	return modules.RPCPriceTable{
		Validity:             time.Minute,
		FundAccountCost:      oneCurrency,
		UpdatePriceTableCost: oneCurrency,

		HasSectorBaseCost: oneCurrency,
		InitBaseCost:      oneCurrency,
		MemoryTimeCost:    oneCurrency,
		ReadBaseCost:      oneCurrency,
		ReadLengthCost:    oneCurrency,
		SwapSectorCost:    oneCurrency,

		DownloadBandwidthCost: hes.DownloadBandwidthPrice,
		UploadBandwidthCost:   hes.UploadBandwidthPrice,

		WriteBaseCost:   oneCurrency,
		WriteLengthCost: oneCurrency,
		WriteStoreCost:  oneCurrency,
	}
}
