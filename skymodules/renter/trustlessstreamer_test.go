package renter

import (
	"bytes"
	"testing"

	"gitlab.com/NebulousLabs/fastrand"
	"gitlab.com/SkynetLabs/skyd/skymodules"
	"go.sia.tech/siad/crypto"
	"go.sia.tech/siad/modules"
)

// TestBuildSubSectionProof is a unit test for BuildSubSectionProof which builds
// a proof for every possible segment of a section.
func TestBuildSubSectionProof(t *testing.T) {
	ec, err := skymodules.NewRSSubCode(2, 3, crypto.SegmentSize)
	if err != nil {
		t.Fatal(err)
	}

	// Create the encoded chunk.
	pieceSize := modules.SectorSize //uint64(2 << 14) // 16 kib
	chunkSize := pieceSize * uint64(ec.MinPieces())
	chunk0 := fastrand.Bytes(int(chunkSize))
	chunk1 := fastrand.Bytes(int(chunkSize))
	logicalChunkData0, err := ec.Encode(append([]byte{}, chunk0...)) // deep-copy to not modify 'chunk'
	if err != nil {
		t.Fatal(err)
	}
	logicalChunkData1, err := ec.Encode(append([]byte{}, chunk1...)) // deep-copy to not modify 'chunk'
	if err != nil {
		t.Fatal(err)
	}
	chunks := [][]byte{chunk0, chunk1}
	logicalChunkDatas := [][][]byte{logicalChunkData0, logicalChunkData1}

	// Get number of sections per chunk.
	requestSize := SkylinkDataSourceRequestSize
	numSections := chunkSize / requestSize
	if chunkSize%requestSize != 0 {
		t.Fatal("chunkSize should be cleanly divided by request size")
	}

	// Run the test for every potential section of the chunks.
	for chunkIndex, chunk := range chunks {
		logicalChunkData := logicalChunkDatas[chunkIndex]

		for sectionIndex := uint64(0); sectionIndex < numSections; sectionIndex++ {
			// Compute the offset and length of the section within the
			// chunk.
			offsetInChunk := requestSize * sectionIndex
			lengthInChunk := requestSize

			// Trim the logical chunk data to only contain the parts of the
			// pieces relevant to the section.
			lcd := make([][]byte, len(logicalChunkData))
			pieceOff, pieceLen := GetPieceOffsetAndLen(ec, offsetInChunk, lengthInChunk)
			for pieceIndex := range lcd {
				lcd[pieceIndex] = logicalChunkData[pieceIndex][pieceOff : pieceOff+pieceLen]
			}

			// Create the proofs for each piece. That is the proof that the
			// section is part of the sector.
			sectionProofStart := int(pieceOff) / crypto.SegmentSize
			sectionProofEnd := int(pieceOff+pieceLen) / crypto.SegmentSize
			var proofs [][]crypto.Hash
			for _, piece := range logicalChunkData {
				proof := crypto.MerkleRangeProof(piece, sectionProofStart, sectionProofEnd)
				proofs = append(proofs, proof)
			}

			// The proofs should be valid.
			for pieceIndex, piece := range lcd {
				merkleRoot := crypto.MerkleRoot(logicalChunkData[pieceIndex])
				valid := crypto.VerifyRangeProof(piece, proofs[pieceIndex], int(pieceOff)/crypto.SegmentSize, int(pieceOff+pieceLen)/crypto.SegmentSize, merkleRoot)
				if !valid {
					t.Fatal("invalid merkle proof")
				}
			}

			// Build the download response.
			dr := newDownloadResponse(offsetInChunk, lengthInChunk, ec, lcd, proofs, nil)

			// The download response should be able to recover the right
			// data.
			section, err := dr.externDownloadedData.Recover()
			if err != nil {
				t.Fatal(err)
			}
			if !bytes.Equal(section, chunk[offsetInChunk:offsetInChunk+lengthInChunk]) {
				t.Fatal("mismatch", len(section), lengthInChunk)
			}

			// For every segment of every logical piece within the downloaded data, create a
			// proof that proves this segment is contained within a section
			// by combining the section proof and segment proof using
			// ComputeSectionProof.
			for pieceIndex := range dr.externDownloadedData.Proofs {
				// Compute the merkle root of the logical piece.
				merkleRoot := crypto.MerkleRoot(logicalChunkData[pieceIndex])

				// Grab the section proof. This proves that the section
				// is part of a sector.
				sectionProof := dr.externDownloadedData.Proofs[pieceIndex]

				// Build one proof for every segment of the section.
				for startSegment := 0; startSegment < len(lcd[pieceIndex])/crypto.SegmentSize; startSegment++ {
					// Create the subSectionProof. This proves that
					// the downloaded data is part of the section.
					subSectionProof := crypto.MerkleRangeProof(lcd[pieceIndex], startSegment, startSegment+1)

					// Build the combined proof of section and
					// sub-section proof. This proves that the
					// downloaded data is part of a sector.
					combinedProof := BuildSubSectionProof(sectionIndex, subSectionProof, sectionProof)

					// Verify the proof.
					segmentProofStart := sectionProofStart + startSegment
					segmentProofEnd := segmentProofStart + 1
					logicalPiece := lcd[pieceIndex]
					valid := crypto.VerifyRangeProof(logicalPiece[startSegment*crypto.SegmentSize:][:crypto.SegmentSize], combinedProof, segmentProofStart, segmentProofEnd, merkleRoot)
					if !valid {
						t.Fatal("invalid merkle proof")
					}
				}
			}
		}
	}
}
