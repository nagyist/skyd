package gouging

import (
	"strings"
	"testing"
	"time"

	"gitlab.com/NebulousLabs/errors"
	"gitlab.com/SkynetLabs/skyd/skymodules"
	"go.sia.tech/siad/modules"
	"go.sia.tech/siad/types"
)

// TestCheckDownload checks that the fetch backups price gouging
// checker is correctly detecting price gouging from a host.
func TestCheckDownload(t *testing.T) {
	oneCurrency := types.NewCurrency64(1)

	// minAllowance contains only the fields necessary to test the price gouging
	// function. The min allowance doesn't include any of the max prices,
	// because the function will ignore them if they are not set.
	minAllowance := skymodules.Allowance{
		// Funds is set such that the tests come out to an easy, round number.
		// One siacoin is multiplied by the number of elements that are checked
		// for gouging, and then divided by the gounging denominator.
		Funds: types.SiacoinPrecision.Mul64(2).Add(oneCurrency).Div64(downloadGougingFractionDenom).Sub(oneCurrency),

		ExpectedDownload: skymodules.StreamDownloadSize, // 1 stream download operation.
	}
	// minHostSettings contains only the fields necessary to test the price
	// gouging function.
	//
	// The cost is set to be exactly equal to the price gouging limit, such that
	// slightly decreasing any of the values evades the price gouging detector.
	minPriceTable := modules.RPCPriceTable{
		ReadBaseCost:          types.SiacoinPrecision,
		ReadLengthCost:        oneCurrency,
		DownloadBandwidthCost: types.SiacoinPrecision.Div64(skymodules.StreamDownloadSize),
	}

	err := CheckDownload(minAllowance, minPriceTable)
	if err == nil {
		t.Fatal("expecting price gouging check to fail:", err)
	}

	// Drop the host prices one field at a time.
	newPriceTable := minPriceTable
	newPriceTable.ReadBaseCost = minPriceTable.ReadBaseCost.Mul64(100).Div64(101)
	err = CheckDownload(minAllowance, newPriceTable)
	if err != nil {
		t.Fatal(err)
	}
	newPriceTable = minPriceTable
	newPriceTable.DownloadBandwidthCost = minPriceTable.DownloadBandwidthCost.Mul64(100).Div64(101)
	err = CheckDownload(minAllowance, newPriceTable)
	if err != nil {
		t.Fatal(err)
	}

	// Set min settings on the allowance that are just below what should be
	// acceptable.
	maxAllowance := minAllowance
	maxAllowance.Funds = maxAllowance.Funds.Add(oneCurrency)
	maxAllowance.MaxRPCPrice = modules.MDMReadCost(&minPriceTable, skymodules.StreamDownloadSize).Add(oneCurrency)
	maxAllowance.MaxContractPrice = oneCurrency
	maxAllowance.MaxDownloadBandwidthPrice = minPriceTable.DownloadBandwidthCost.Add(oneCurrency)
	maxAllowance.MaxStoragePrice = oneCurrency
	maxAllowance.MaxUploadBandwidthPrice = oneCurrency

	// The max allowance should have no issues with price gouging.
	err = CheckDownload(maxAllowance, minPriceTable)
	if err != nil {
		t.Fatal(err)
	}

	// Should fail if the MaxRPCPrice is dropped.
	failAllowance := maxAllowance
	failAllowance.MaxRPCPrice = oneCurrency
	err = CheckDownload(failAllowance, minPriceTable)
	if err == nil {
		t.Fatal("expecting price gouging check to fail")
	}

	// Should fail if the MaxDownloadBandwidthPrice is dropped.
	failAllowance = maxAllowance
	failAllowance.MaxDownloadBandwidthPrice = minPriceTable.DownloadBandwidthCost.Sub(oneCurrency)
	err = CheckDownload(failAllowance, minPriceTable)
	if err == nil {
		t.Fatal("expecting price gouging check to fail")
	}
}

// TestCheckPCWS checks that the PCWS gouging check is triggering at the right
// times.
func TestCheckPCWS(t *testing.T) {
	// Create some defaults to get some intuitive ideas for gouging.
	//
	// 100 workers and 1e9 expected download means ~2e6 HasSector queries will
	// be performed.
	pt := modules.RPCPriceTable{
		InitBaseCost:          types.NewCurrency64(1e3),
		DownloadBandwidthCost: types.NewCurrency64(1e3),
		UploadBandwidthCost:   types.NewCurrency64(1e3),
		HasSectorBaseCost:     types.NewCurrency64(1e6),
	}
	allowance := skymodules.Allowance{
		MaxDownloadBandwidthPrice: types.NewCurrency64(2e3),
		MaxUploadBandwidthPrice:   types.NewCurrency64(2e3),

		Funds: types.NewCurrency64(1e18),

		ExpectedDownload: 1e9, // 1 GiB
	}
	numWorkers := 100
	numRoots := 30

	// Check that the gouging passes for normal values.
	err := CheckPCWS(allowance, pt, numWorkers, numRoots)
	if err != nil {
		t.Error(err)
	}

	// Check with high init base cost.
	pt.InitBaseCost = types.NewCurrency64(1e12)
	err = CheckPCWS(allowance, pt, numWorkers, numRoots)
	if err == nil {
		t.Error("bad")
	}
	pt.InitBaseCost = types.NewCurrency64(1e3)

	// Check with high upload bandwidth cost.
	pt.UploadBandwidthCost = types.NewCurrency64(1e12)
	err = CheckPCWS(allowance, pt, numWorkers, numRoots)
	if err == nil {
		t.Error("bad")
	}
	pt.UploadBandwidthCost = types.NewCurrency64(1e3)

	// Check with high download bandwidth cost.
	pt.DownloadBandwidthCost = types.NewCurrency64(1e12)
	err = CheckPCWS(allowance, pt, numWorkers, numRoots)
	if err == nil {
		t.Error("bad")
	}
	pt.DownloadBandwidthCost = types.NewCurrency64(1e3)

	// Check with high HasSector cost.
	pt.HasSectorBaseCost = types.NewCurrency64(1e12)
	err = CheckPCWS(allowance, pt, numWorkers, numRoots)
	if err == nil {
		t.Error("bad")
	}
	pt.HasSectorBaseCost = types.NewCurrency64(1e6)

	// Check with low MaxDownloadBandwidthPrice.
	allowance.MaxDownloadBandwidthPrice = types.NewCurrency64(100)
	err = CheckPCWS(allowance, pt, numWorkers, numRoots)
	if err == nil {
		t.Error("bad")
	}
	allowance.MaxDownloadBandwidthPrice = types.NewCurrency64(2e3)

	// Check with low MaxUploadBandwidthPrice.
	allowance.MaxUploadBandwidthPrice = types.NewCurrency64(100)
	err = CheckPCWS(allowance, pt, numWorkers, numRoots)
	if err == nil {
		t.Error("bad")
	}
	allowance.MaxUploadBandwidthPrice = types.NewCurrency64(2e3)

	// Check with reduced funds.
	allowance.Funds = types.NewCurrency64(1e15)
	err = CheckPCWS(allowance, pt, numWorkers, numRoots)
	if err == nil {
		t.Error("bad")
	}
	allowance.Funds = types.NewCurrency64(1e18)

	// Check with increased expected download.
	allowance.ExpectedDownload = 1e12
	err = CheckPCWS(allowance, pt, numWorkers, numRoots)
	if err == nil {
		t.Error("bad")
	}
	allowance.ExpectedDownload = 1e9

	// Check that the base allowanace still passes. (ensures values have been
	// reset correctly)
	err = CheckPCWS(allowance, pt, numWorkers, numRoots)
	if err != nil {
		t.Error(err)
	}
}

// TestCheckProjectDownload checks that `CheckProjectDownloadGouging` is
// correctly detecting price gouging from a host.
func TestCheckProjectDownload(t *testing.T) {
	t.Parallel()

	// allowance contains only the fields necessary to test the price gouging
	hes := modules.DefaultHostExternalSettings()
	allowance := skymodules.Allowance{
		Funds:                     types.SiacoinPrecision.Mul64(1e3),
		MaxDownloadBandwidthPrice: hes.DownloadBandwidthPrice.Mul64(10),
		MaxUploadBandwidthPrice:   hes.UploadBandwidthPrice.Mul64(10),
	}

	// verify happy case
	pt := newDefaultPriceTable()
	err := CheckProjectDownload(allowance, pt)
	if err != nil {
		t.Fatal("unexpected price gouging failure", err)
	}

	// verify max download bandwidth price gouging
	pt = newDefaultPriceTable()
	pt.DownloadBandwidthCost = allowance.MaxDownloadBandwidthPrice.Add64(1)
	err = CheckProjectDownload(allowance, pt)
	if err == nil || !strings.Contains(err.Error(), "download bandwidth price") {
		t.Fatalf("expected download bandwidth price gouging error, instead error was '%v'", err)
	}

	// verify max upload bandwidth price gouging
	pt = newDefaultPriceTable()
	pt.UploadBandwidthCost = allowance.MaxUploadBandwidthPrice.Add64(1)
	err = CheckProjectDownload(allowance, pt)
	if err == nil || !strings.Contains(err.Error(), "upload bandwidth price") {
		t.Fatalf("expected upload bandwidth price gouging error, instead error was '%v'", err)
	}

	// update the expected download to be non zero and verify the default prices
	allowance.ExpectedDownload = 1 << 30 // 1GiB
	pt = newDefaultPriceTable()
	err = CheckProjectDownload(allowance, pt)
	if err != nil {
		t.Fatal("unexpected price gouging failure", err)
	}

	// verify gouging of MDM related costs, in order to verify if gouging
	// detection kicks in we need to ensure the cost of executing enough PDBRs
	// to fulfil the expected download exceeds the allowance

	// we do this by maxing out the upload and bandwidth costs and setting all
	// default cost components to 250 pS, note that this value is arbitrary,
	// setting those costs at 250 pS simply proved to push the price per PDBR
	// just over the allowed limit.
	//
	// Cost breakdown:
	// - cost per PDBR 266.4 mS
	// - total cost to fulfil expected download 4.365 KS
	// - reduced cost after applying downloadGougingFractionDenom: 1.091 KS
	// - exceeding the allowance of 1 KS, which is what we are after
	pt.UploadBandwidthCost = allowance.MaxUploadBandwidthPrice
	pt.DownloadBandwidthCost = allowance.MaxDownloadBandwidthPrice
	pS := types.SiacoinPrecision.MulFloat(1e-12)
	pt.InitBaseCost = pt.InitBaseCost.Add(pS.Mul64(250))
	pt.ReadBaseCost = pt.ReadBaseCost.Add(pS.Mul64(250))
	pt.MemoryTimeCost = pt.MemoryTimeCost.Add(pS.Mul64(250))
	err = CheckProjectDownload(allowance, pt)
	if err == nil || !strings.Contains(err.Error(), "combined PDBR pricing of host yields") {
		t.Fatalf("expected PDBR price gouging error, instead error was '%v'", err)
	}

	// verify these checks are ignored if the funds are 0
	allowance.Funds = types.ZeroCurrency
	err = CheckProjectDownload(allowance, pt)
	if err != nil {
		t.Fatal("unexpected price gouging failure", err)
	}

	allowance.Funds = types.SiacoinPrecision.Mul64(1e3) // reset

	// verify bumping every individual cost component to an insane value results
	// in a price gouging error
	pt = newDefaultPriceTable()
	pt.InitBaseCost = types.SiacoinPrecision.Mul64(100)
	err = CheckProjectDownload(allowance, pt)
	if err == nil || !strings.Contains(err.Error(), "combined PDBR pricing of host yields") {
		t.Fatalf("expected PDBR price gouging error, instead error was '%v'", err)
	}

	pt = newDefaultPriceTable()
	pt.ReadBaseCost = types.SiacoinPrecision
	err = CheckProjectDownload(allowance, pt)
	if err == nil || !strings.Contains(err.Error(), "combined PDBR pricing of host yields") {
		t.Fatalf("expected PDBR price gouging error, instead error was '%v'", err)
	}

	pt = newDefaultPriceTable()
	pt.ReadLengthCost = types.SiacoinPrecision
	err = CheckProjectDownload(allowance, pt)
	if err == nil || !strings.Contains(err.Error(), "combined PDBR pricing of host yields") {
		t.Fatalf("expected PDBR price gouging error, instead error was '%v'", err)
	}

	pt = newDefaultPriceTable()
	pt.MemoryTimeCost = types.SiacoinPrecision
	err = CheckProjectDownload(allowance, pt)
	if err == nil || !strings.Contains(err.Error(), "combined PDBR pricing of host yields") {
		t.Fatalf("expected PDBR price gouging error, instead error was '%v'", err)
	}
}

// TestCheckSnapshot checks that the download snapshot price gouging checker is
// correctly detecting price gouging from a host.
func TestCheckSnapshot(t *testing.T) {
	hes := modules.DefaultHostExternalSettings()

	allowance := skymodules.DefaultAllowance
	allowance.Funds = types.SiacoinPrecision.Mul64(1e3)
	allowance.MaxDownloadBandwidthPrice = hes.DownloadBandwidthPrice.Mul64(2)
	allowance.MaxUploadBandwidthPrice = hes.UploadBandwidthPrice.Mul64(2)
	allowance.ExpectedDownload = 1 << 30 // 1GiB

	priceTable := modules.RPCPriceTable{
		ReadBaseCost:          hes.SectorAccessPrice,
		ReadLengthCost:        types.NewCurrency64(1),
		InitBaseCost:          hes.BaseRPCPrice,
		DownloadBandwidthCost: hes.DownloadBandwidthPrice,
		UploadBandwidthCost:   hes.UploadBandwidthPrice,
	}

	// verify basic case
	err := CheckSnapshot(allowance, priceTable)
	if err != nil {
		t.Fatal("unexpected failure", err)
	}

	// verify high init costs
	gougingPriceTable := priceTable
	gougingPriceTable.InitBaseCost = types.SiacoinPrecision
	gougingPriceTable.ReadBaseCost = types.SiacoinPrecision
	err = CheckSnapshot(allowance, gougingPriceTable)
	if err == nil {
		t.Fatal("unexpected outcome", err)
	}

	// verify DL bandwidth gouging
	gougingPriceTable = priceTable
	gougingPriceTable.DownloadBandwidthCost = allowance.MaxDownloadBandwidthPrice.Mul64(2)
	err = CheckSnapshot(allowance, gougingPriceTable)
	if err == nil {
		t.Fatal("unexpected outcome", err)
	}
}

// TestCheckSubscription is a unit test for CheckSubscriptionGouging.
func TestCheckSubscription(t *testing.T) {
	// Create a pricetable and allowance which are reasonable.
	a := skymodules.DefaultAllowance
	pt := modules.RPCPriceTable{
		SubscriptionMemoryCost:       types.NewCurrency64(1),
		SubscriptionNotificationCost: types.NewCurrency64(1),
	}

	// Should work.
	if err := CheckSubscription(a, pt); err != nil {
		t.Fatal(err)
	}

	// High memory cost shouldn't work.
	ptHighMemCost := pt
	ptHighMemCost.SubscriptionMemoryCost = types.NewCurrency64(2)
	if err := CheckSubscription(a, ptHighMemCost); !errors.Contains(err, errHighSubscriptionMemoryCost) {
		t.Fatal(err)
	}

	// High notification cost shouldn't work.
	ptHighNotificationCost := pt
	ptHighNotificationCost.SubscriptionNotificationCost = types.NewCurrency64(2)
	if err := CheckSubscription(a, ptHighNotificationCost); !errors.Contains(err, errHighSubscriptionNotificationCost) {
		t.Fatal(err)
	}

	// High bandwidth cost should be caught by CheckProjectDownloadGouging
	// at the end.
	ptHighBandwidthCost := pt
	ptHighBandwidthCost.DownloadBandwidthCost = types.SiacoinPrecision.Mul64(1e9)
	if err := CheckSubscription(a, ptHighBandwidthCost); err == nil {
		t.Fatal("should fail")
	}
}

// TestCheckUpload checks that the upload price gouging checker is correctly
// detecting price gouging from a host.
func TestCheckUpload(t *testing.T) {
	oneCurrency := types.NewCurrency64(1)

	// Check default allowance and default PT. Should work.
	defaultPT := newDefaultPriceTable()
	allowance := skymodules.DefaultAllowance
	err := CheckUpload(allowance, defaultPT)
	if err != nil {
		t.Fatal(err)
	}

	// minAllowance contains only the fields necessary to test the price gouging
	// function. The min allowance doesn't include any of the max prices,
	// because the function will ignore them if they are not set.
	minAllowance := skymodules.Allowance{
		// Funds is set such that the tests come out to an easy, round number.
		// One siacoin is multiplied by the number of elements that are checked
		// for gouging, and then divided by the gounging denominator.
		Funds:  types.SiacoinPrecision.Mul64(4).Div64(uploadGougingFractionDenom).Sub(oneCurrency),
		Period: 1, // 1 block.

		ExpectedStorage: modules.SectorSize, // 1 append

		MaxRPCPrice:          types.SiacoinPrecision,
		MaxSectorAccessPrice: types.SiacoinPrecision,
		MaxStoragePrice:      types.SiacoinPrecision,
	}

	// Compute the right funds for the minAllowance given the price table.
	tb := modules.NewProgramBuilder(&defaultPT, minAllowance.Period)
	err = tb.AddAppendInstruction(make([]byte, modules.SectorSize), true, 0)
	if err != nil {
		t.Fatal(err)
	}
	cost, _, _ := tb.Cost(true)
	bandwidthCost := modules.MDMBandwidthCost(defaultPT, modules.SectorSize+2920, 2920)
	cost = cost.Add(bandwidthCost)

	fullCostPerByte := cost.Div64(modules.SectorSize)
	allowanceStorageCost := fullCostPerByte.Mul64(minAllowance.ExpectedStorage)
	minAllowance.Funds = allowanceStorageCost.Div64(uploadGougingFractionDenom)

	// Check the minAllowance and defaultPT. Should work.
	err = CheckUpload(minAllowance, defaultPT)
	if err != nil {
		t.Fatal(err)
	}

	// Increase each setting in the price table by a delta to cause
	// the gouging check to fail. The delta is set to gouging denominator to
	// avoid integer rounding errors.
	delta := uint64(uploadGougingFractionDenom)
	failPT := defaultPT
	failPT.InitBaseCost = failPT.InitBaseCost.Add64(delta)
	err = CheckUpload(minAllowance, failPT)
	if err == nil {
		t.Fatal("should fail")
	}
	failPT = defaultPT
	failPT.WriteBaseCost = failPT.WriteBaseCost.Add64(delta)
	err = CheckUpload(minAllowance, failPT)
	if err == nil {
		t.Fatal("should fail")
	}
	failPT = defaultPT
	failPT.WriteStoreCost = failPT.WriteStoreCost.Add64(delta)
	err = CheckUpload(minAllowance, failPT)
	if err == nil {
		t.Fatal("should fail")
	}
	failPT = defaultPT
	failPT.UploadBandwidthCost = failPT.UploadBandwidthCost.Add64(delta)
	err = CheckUpload(minAllowance, failPT)
	if err == nil {
		t.Fatal("should fail")
	}
	failPT = defaultPT
	failPT.DownloadBandwidthCost = failPT.DownloadBandwidthCost.Add64(delta)
	err = CheckUpload(minAllowance, failPT)
	if err == nil {
		t.Fatal("should fail")
	}

	// minPT is set to exactly the limits in the allowance set by the user.
	// Increasing any of the fields will result in gouging failures.
	minPT := modules.RPCPriceTable{
		MemoryTimeCost:        oneCurrency,
		WriteLengthCost:       oneCurrency,
		InitBaseCost:          minAllowance.MaxRPCPrice,
		WriteBaseCost:         minAllowance.MaxSectorAccessPrice,
		WriteStoreCost:        minAllowance.MaxStoragePrice,
		UploadBandwidthCost:   minAllowance.MaxUploadBandwidthPrice,
		DownloadBandwidthCost: minAllowance.MaxDownloadBandwidthPrice,
	}

	failPT = minPT
	failPT.MemoryTimeCost = failPT.MemoryTimeCost.Add64(1)
	err = CheckUpload(minAllowance, failPT)
	if err == nil {
		t.Fatal("should fail")
	}
	failPT = minPT
	failPT.WriteLengthCost = failPT.WriteLengthCost.Add64(1)
	err = CheckUpload(minAllowance, failPT)
	if err == nil {
		t.Fatal("should fail")
	}
	failPT = minPT
	failPT.InitBaseCost = failPT.InitBaseCost.Add64(1)
	err = CheckUpload(minAllowance, failPT)
	if err == nil {
		t.Fatal("should fail")
	}
	failPT = minPT
	failPT.WriteBaseCost = failPT.WriteBaseCost.Add64(1)
	err = CheckUpload(minAllowance, failPT)
	if err == nil {
		t.Fatal("should fail")
	}
	failPT = minPT
	failPT.WriteStoreCost = failPT.WriteStoreCost.Add64(1)
	err = CheckUpload(minAllowance, failPT)
	if err == nil {
		t.Fatal("should fail")
	}
	failPT = minPT
	failPT.UploadBandwidthCost = failPT.UploadBandwidthCost.Add64(1)
	err = CheckUpload(minAllowance, failPT)
	if err == nil {
		t.Fatal("should fail")
	}
	failPT = minPT
	failPT.DownloadBandwidthCost = failPT.DownloadBandwidthCost.Add64(1)
	err = CheckUpload(minAllowance, failPT)
	if err == nil {
		t.Fatal("should fail")
	}

	// Make download gouging check fail once to verify we are calling it.
	failPT = minPT
	failPT.ReadLengthCost = types.NewCurrency64(2)
	err = CheckUpload(minAllowance, failPT)
	if err == nil || !strings.Contains(err.Error(), "failed download gouging check") {
		t.Error("expecting price gouging check to fail")
	}
}

// TestCheckUploadHeS checks that the upload price gouging checker is
// correctly detecting price gouging from a host.
func TestCheckUploadHeS(t *testing.T) {
	oneCurrency := types.NewCurrency64(1)

	// minAllowance contains only the fields necessary to test the price gouging
	// function. The min allowance doesn't include any of the max prices,
	// because the function will ignore them if they are not set.
	minAllowance := skymodules.Allowance{
		// Funds is set such that the tests come out to an easy, round number.
		// One siacoin is multiplied by the number of elements that are checked
		// for gouging, and then divided by the gounging denominator.
		Funds:  types.SiacoinPrecision.Mul64(4).Div64(uploadGougingFractionDenom).Sub(oneCurrency),
		Period: 1, // 1 block.

		ExpectedStorage: skymodules.StreamUploadSize, // 1 stream upload operation.
	}
	// minHostSettings contains only the fields necessary to test the price
	// gouging function.
	//
	// The cost is set to be exactly equal to the price gouging limit, such that
	// slightly decreasing any of the values evades the price gouging detector.
	minHostSettings := modules.HostExternalSettings{
		BaseRPCPrice:         types.SiacoinPrecision,
		SectorAccessPrice:    types.SiacoinPrecision,
		UploadBandwidthPrice: types.SiacoinPrecision.Div64(skymodules.StreamUploadSize),
		StoragePrice:         types.SiacoinPrecision.Div64(skymodules.StreamUploadSize),
	}

	// minPriceTable has all of the costs set to 0 (except ReadLengthCost)
	// to make sure the download gouging check within the upload gouging
	// check doesn't fail.
	minPriceTable := &modules.RPCPriceTable{
		ReadLengthCost: oneCurrency,
	}

	err := CheckUploadHES(minAllowance, minPriceTable, minHostSettings, false)
	if err == nil {
		t.Fatal("expecting price gouging check to fail:", err)
	}

	// Drop the host prices one field at a time.
	newHostSettings := minHostSettings
	newHostSettings.BaseRPCPrice = minHostSettings.BaseRPCPrice.Mul64(100).Div64(101)
	err = CheckUploadHES(minAllowance, minPriceTable, newHostSettings, false)
	if err != nil {
		t.Fatal(err)
	}
	newHostSettings = minHostSettings
	newHostSettings.SectorAccessPrice = minHostSettings.SectorAccessPrice.Mul64(100).Div64(101)
	err = CheckUploadHES(minAllowance, minPriceTable, newHostSettings, false)
	if err != nil {
		t.Fatal(err)
	}
	newHostSettings = minHostSettings
	newHostSettings.UploadBandwidthPrice = minHostSettings.UploadBandwidthPrice.Mul64(100).Div64(101)
	err = CheckUploadHES(minAllowance, minPriceTable, newHostSettings, false)
	if err != nil {
		t.Fatal(err)
	}
	newHostSettings = minHostSettings
	newHostSettings.StoragePrice = minHostSettings.StoragePrice.Mul64(100).Div64(101)
	err = CheckUploadHES(minAllowance, minPriceTable, newHostSettings, false)
	if err != nil {
		t.Fatal(err)
	}

	// Set min settings on the allowance that are just below that should be
	// acceptable.
	maxAllowance := minAllowance
	maxAllowance.Funds = maxAllowance.Funds.Add(oneCurrency)
	maxAllowance.MaxRPCPrice = types.SiacoinPrecision.Add(oneCurrency)
	maxAllowance.MaxContractPrice = oneCurrency
	maxAllowance.MaxDownloadBandwidthPrice = oneCurrency
	maxAllowance.MaxSectorAccessPrice = types.SiacoinPrecision.Add(oneCurrency)
	maxAllowance.MaxStoragePrice = types.SiacoinPrecision.Div64(skymodules.StreamUploadSize).Add(oneCurrency)
	maxAllowance.MaxUploadBandwidthPrice = types.SiacoinPrecision.Div64(skymodules.StreamUploadSize).Add(oneCurrency)

	// The max allowance should have no issues with price gouging.
	err = CheckUploadHES(maxAllowance, minPriceTable, minHostSettings, false)
	if err != nil {
		t.Fatal(err)
	}

	// Should fail if the MaxRPCPrice is dropped.
	failAllowance := maxAllowance
	failAllowance.MaxRPCPrice = types.SiacoinPrecision.Sub(oneCurrency)
	err = CheckUploadHES(failAllowance, minPriceTable, minHostSettings, false)
	if err == nil {
		t.Error("expecting price gouging check to fail")
	}

	// Should fail if the MaxSectorAccessPrice is dropped.
	failAllowance = maxAllowance
	failAllowance.MaxSectorAccessPrice = types.SiacoinPrecision.Sub(oneCurrency)
	err = CheckUploadHES(failAllowance, minPriceTable, minHostSettings, false)
	if err == nil {
		t.Error("expecting price gouging check to fail")
	}

	// Should fail if the MaxStoragePrice is dropped.
	failAllowance = maxAllowance
	failAllowance.MaxStoragePrice = types.SiacoinPrecision.Div64(skymodules.StreamUploadSize).Sub(oneCurrency)
	err = CheckUploadHES(failAllowance, minPriceTable, minHostSettings, false)
	if err == nil {
		t.Error("expecting price gouging check to fail")
	}

	// Should fail if the MaxUploadBandwidthPrice is dropped.
	failAllowance = maxAllowance
	failAllowance.MaxUploadBandwidthPrice = types.SiacoinPrecision.Div64(skymodules.StreamUploadSize).Sub(oneCurrency)
	err = CheckUploadHES(failAllowance, minPriceTable, minHostSettings, false)
	if err == nil {
		t.Error("expecting price gouging check to fail")
	}

	// Make download gouging check fail once to verify we are calling it.
	failPT := minPriceTable
	failPT.ReadLengthCost = types.NewCurrency64(2)
	failAllowance.MaxSectorAccessPrice = types.SiacoinPrecision.Sub(oneCurrency)
	err = CheckUploadHES(minAllowance, failPT, minHostSettings, false)
	if err == nil || !strings.Contains(err.Error(), "failed download gouging check") {
		t.Error("expecting price gouging check to fail")
	}

	// Download gouging check can't fail without pricetable.
	newHostSettings = minHostSettings
	newHostSettings.SectorAccessPrice = minHostSettings.SectorAccessPrice.Mul64(100).Div64(101)
	err = CheckUploadHES(minAllowance, nil, newHostSettings, true)
	if err != nil {
		t.Error(err)
	}

	// Should fail if no pricetable was provided when allowSkipPT is
	// 'false'.
	failPT.ReadLengthCost = types.NewCurrency64(2)
	err = CheckUploadHES(minAllowance, nil, newHostSettings, false)
	if err == nil || !strings.Contains(err.Error(), "pricetable missing") {
		t.Error("expecting price gouging check to fail")
	}
}

// newDefaultPriceTable is a helper function that returns a price table with
// default prices for all fields
func newDefaultPriceTable() modules.RPCPriceTable {
	hes := modules.DefaultHostExternalSettings()
	oneCurrency := types.NewCurrency64(1)
	return modules.RPCPriceTable{
		Validity:             time.Minute,
		FundAccountCost:      oneCurrency,
		UpdatePriceTableCost: oneCurrency,

		HasSectorBaseCost: oneCurrency,
		InitBaseCost:      oneCurrency,
		MemoryTimeCost:    oneCurrency,
		ReadBaseCost:      oneCurrency,
		ReadLengthCost:    oneCurrency,
		SwapSectorCost:    oneCurrency,

		DownloadBandwidthCost: hes.DownloadBandwidthPrice,
		UploadBandwidthCost:   hes.UploadBandwidthPrice,

		WriteBaseCost:   oneCurrency,
		WriteLengthCost: oneCurrency,
		WriteStoreCost:  oneCurrency,
	}
}
