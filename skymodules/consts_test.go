package skymodules

import (
	"testing"
	"time"
)

// TestDaysInMonth tests the DaysInMonth function
func TestDaysInMonth(t *testing.T) {
	var tests = []struct {
		year  int
		month time.Month
		day   int
	}{
		{2022, time.January, 31},
		{2022, time.February, 28},
		{2020, time.February, 29},
		{2022, time.March, 31},
		{2022, time.April, 30},
		{2022, time.May, 31},
		{2022, time.June, 30},
		{2022, time.July, 31},
		{2022, time.August, 31},
		{2022, time.September, 30},
		{2022, time.October, 31},
		{2022, time.November, 30},
		{2022, time.December, 31},
	}
	for _, test := range tests {
		day := DaysInMonth(test.month, test.year)
		if test.day != day {
			t.Fatal(test.day, "!=", day)
		}
	}
}
