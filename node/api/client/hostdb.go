package client

import (
	"encoding/json"

	"gitlab.com/SkynetLabs/skyd/node/api"
	"gitlab.com/SkynetLabs/skyd/skymodules"
	"go.sia.tech/siad/types"
)

// HostDbGet requests the /hostdb endpoint's resources.
func (c *Client) HostDbGet() (hdg api.HostdbGet, err error) {
	err = c.get("/hostdb", &hdg)
	return
}

// HostDbActiveGet requests the /hostdb/active endpoint's resources.
func (c *Client) HostDbActiveGet() (hdag api.HostdbActiveGET, err error) {
	err = c.get("/hostdb/active", &hdag)
	return
}

// HostDbAllGet requests the /hostdb/all endpoint's resources.
func (c *Client) HostDbAllGet() (hdag api.HostdbAllGET, err error) {
	err = c.get("/hostdb/all", &hdag)
	return
}

// HostDbBlockedDomainsGet requests the /hostdb/blockeddomains endpoint's resources.
func (c *Client) HostDbBlockedDomainsGet() (hdbdg api.HostdbBlockedDomainsGET, err error) {
	err = c.get("/hostdb/blockeddomains", &hdbdg)
	return
}

// HostDbBlockDomainsPost requests the /hostdb/blockdomains POST endpoint.
func (c *Client) HostDbBlockDomainsPost(domains []string) (err error) {
	hdbdp := api.HostdbBlockDomainsPOST{
		Domains: domains,
	}
	data, err := json.Marshal(hdbdp)
	if err != nil {
		return err
	}
	err = c.post("/hostdb/blockdomains", string(data), nil)
	return
}

// HostDbFilterModeGet requests the /hostdb/filtermode GET endpoint
func (c *Client) HostDbFilterModeGet() (hdfmg api.HostdbFilterModeGET, err error) {
	err = c.get("/hostdb/filtermode", &hdfmg)
	return
}

// HostDbFilterModePost requests the /hostdb/filtermode POST endpoint
func (c *Client) HostDbFilterModePost(fm skymodules.FilterMode, hosts []types.SiaPublicKey) (err error) {
	filterMode := fm.String()
	hdblp := api.HostdbFilterModePOST{
		FilterMode: filterMode,
		Hosts:      hosts,
	}

	data, err := json.Marshal(hdblp)
	if err != nil {
		return err
	}
	err = c.post("/hostdb/FilterMode", string(data), nil)
	return
}

// HostDbHostsGet request the /hostdb/hosts/:pubkey endpoint's resources.
func (c *Client) HostDbHostsGet(pk types.SiaPublicKey) (hhg api.HostdbHostsGET, err error) {
	err = c.get("/hostdb/hosts/"+pk.String(), &hhg)
	return
}

// HostDbUnblockDomainsPost requests the /hostdb/unblockdomains POST endpoint.
func (c *Client) HostDbUnblockDomainsPost(domains []string) (err error) {
	hdudp := api.HostdbUnblockDomainsPOST{
		Domains: domains,
	}
	data, err := json.Marshal(hdudp)
	if err != nil {
		return err
	}
	err = c.post("/hostdb/unblockdomains", string(data), nil)
	return
}
