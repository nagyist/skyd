package client

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"io"
	"io/ioutil"
	"net/url"

	"gitlab.com/NebulousLabs/errors"
	"gitlab.com/SkynetLabs/skyd/skymodules"
	"gitlab.com/SkynetLabs/skyd/skymodules/renter"
	"go.sia.tech/core/net/rhp"
	"go.sia.tech/siad/crypto"
	"go.sia.tech/siad/modules"
)

// TrustlessDownload describes an ongoing trustless, streamable download.
// Reading from the download only returns the decoded data of the file but
// verifies the proofs on the fly.
// NOTE: TrustlessDownload is not threadsafe.
type TrustlessDownload struct {
	staticLayout      skymodules.SkyfileLayout
	staticLayoutOff   uint64
	staticResp        io.ReadCloser
	staticSkylinkRoot crypto.Hash

	staticFetchOff   uint64
	currentFetchOff  uint64
	staticFetchLen   uint64
	remainingPayload []byte
}

// SkylinkTrustlessDownload starts a new trustless download of the given
// skylink. The downloaded data can be read from the returned object like from a
// regular download but in the background it will be validated using merkle
// proofs and decoded from logical to physical data.
func (c *Client) SkylinkTrustlessDownload(skylink skymodules.Skylink, offset, length uint64) (*TrustlessDownload, error) {
	values := url.Values{}
	values.Set("offset", fmt.Sprint(offset))
	values.Set("length", fmt.Sprint(length))
	_, r, err := c.getReaderResponse(fmt.Sprintf("/skynet/trustless/file/v1/%v?%v", skylink, values.Encode()))
	if err != nil {
		return nil, err
	}
	return newTrustlessDownload(r, skylink, offset, length)
}

// Close closes the connection.
func (td *TrustlessDownload) Close() error {
	_, _ = io.Copy(ioutil.Discard, td.staticResp) // flush response
	return td.staticResp.Close()
}

// Read implements the io.Reader interface by passing the call on to the
// corresponding handler depending on the type of the file we are downloading.
func (td *TrustlessDownload) Read(b []byte) (int, error) {
	if td.staticLayout.IsSmallFile() {
		return td.readSmallFile(b)
	} else if !td.staticLayout.HasRecursiveFanout(td.staticLayoutOff) {
		return td.readLargeFile(b)
	}
	return td.readExtendedLargeFile(b)
}

// assertMetadata reads the metadata portion of the download and verifies it.
func (td *TrustlessDownload) assertMetadata() error {
	var sectionSize uint64
	err := binary.Read(td.staticResp, binary.LittleEndian, &sectionSize)
	if err != nil {
		return err
	}
	if sectionSize != renter.SkylinkDataSourceRequestSize {
		return errors.New("unsupported section size")
	}
	return nil
}

// readLayout reads the layout from the trustless download response and the
// proof that it is followed up with. Then it verifies the proofs, decodes the
// layout and returns that.
func (td *TrustlessDownload) readLayout() (skymodules.SkyfileLayout, error) {
	// Read the layout data. This will be segment-aligned for the following
	// proof.
	layoutData := make([]byte, segmentAlignedSize(skymodules.SkyfileLayoutSize))
	_, err := io.ReadFull(td.staticResp, layoutData)
	if err != nil {
		return skymodules.SkyfileLayout{}, err
	}

	// Compute the start and end segments of the expected proof.
	proofStartOff, proofEndOff := segmentAlignOffsets(td.staticLayoutOff, skymodules.SkyfileLayoutSize)
	proofStart := proofStartOff / crypto.SegmentSize
	proofEnd := proofEndOff / crypto.SegmentSize

	// Read the proof.
	nHashes := rhp.RangeProofSize(modules.SectorSize/crypto.SegmentSize, proofStart, proofEnd)
	var proof []crypto.Hash
	for i := uint64(0); i < nHashes; i++ {
		var h crypto.Hash
		_, err = io.ReadFull(td.staticResp, h[:])
		if err != nil {
			return skymodules.SkyfileLayout{}, err
		}
		proof = append(proof, h)
	}
	proofValid := crypto.VerifyRangeProof(layoutData, proof, int(proofStart), int(proofEnd), td.staticSkylinkRoot)
	if !proofValid {
		return skymodules.SkyfileLayout{}, errors.New("the proof provided for the layout wasn't valid")
	}

	// Decode the layout.
	var layout skymodules.SkyfileLayout
	layout.Decode(layoutData[:skymodules.SkyfileLayoutSize])
	return layout, nil
}

// readLargeFile handles reading the response of the download of a trustless
// large file. This verifies every section of the download that is returned, the
// corresponding proof that the data is part of the section (unless the whole
// section was returned), the proof that the section is part of a given sector
// and finally that the sector is part of the fanout. Only then we can be sure
// that we were served the right data.
func (td *TrustlessDownload) readLargeFile(b []byte) (int, error) {
	// Check if we have a partial buffered section first.
	n := copy(b, td.remainingPayload)
	td.remainingPayload = td.remainingPayload[n:]
	if n > 0 {
		return n, nil
	} else if n == 0 && td.currentFetchOff >= td.staticFetchOff+td.staticFetchLen {
		return 0, io.EOF
	}

	// Read another section since we don't have any more buffered.
	data, err := td.readLargeFileSection(td.currentFetchOff)
	if err != nil {
		return 0, err
	}
	td.currentFetchOff += uint64(len(data))

	// Return some of the data and add the remaining data to the
	// remaininPayload.
	n = copy(b, data)
	td.remainingPayload = data[n:]
	return n, nil
}

// readLargeFileSection reads the next file section from the download and
// returns its physical data. The currentOffset describes the current offset
// within the file.
func (td *TrustlessDownload) readLargeFileSection(currentOffset uint64) ([]byte, error) {
	// Various helper vars related to indices and offsets within chunks/sections.
	sectionSize := renter.SkylinkDataSourceRequestSize
	currentSection := currentOffset / sectionSize
	offsetInSection := currentOffset % sectionSize
	chunkSize := skymodules.ChunkSize(td.staticLayout.CipherType, uint64(td.staticLayout.FanoutDataPieces))
	chunkIndex := currentSection * sectionSize / chunkSize
	offsetInChunk := (currentSection*sectionSize + offsetInSection) % chunkSize
	if chunkSize%sectionSize != 0 {
		return nil, fmt.Errorf("chunkSize must be section aligned %v mod %v == %v", chunkSize, sectionSize, chunkSize%sectionSize)
	}

	// Get the length and cap it at a section size.
	length := td.staticFetchLen - (td.currentFetchOff - td.staticFetchOff)
	if offsetInSection+length > sectionSize {
		length = sectionSize - offsetInSection
	}

	// Get an erasure coder for the fanout and the number of pieces per
	// chunk to be expected in the fanout.
	fanoutEC, err := skymodules.NewRSSubCode(int(td.staticLayout.FanoutDataPieces), int(td.staticLayout.FanoutParityPieces), crypto.SegmentSize)
	if err != nil {
		return nil, err
	}
	fanoutPiecesPerChunk := td.staticLayout.FanoutPiecesPerChunk()

	// Read all the pieces and verify their proofs.
	pieces := make([][]byte, td.staticLayout.FanoutDataPieces+td.staticLayout.FanoutParityPieces)
	for remainingPieces := td.staticLayout.FanoutDataPieces; remainingPieces > 0; remainingPieces-- {
		// Read the offset of the piece root within the fanout.
		var pieceRootOff uint32
		err := binary.Read(td.staticResp, binary.LittleEndian, &pieceRootOff)
		if err != nil {
			return nil, err
		}

		// Make sure the offset points to the right chunk within the fanout.
		chunkStartOff := td.staticLayout.FanoutOffset(td.staticLayoutOff) + fanoutPiecesPerChunk*chunkIndex*crypto.HashSize
		chunkEndOff := chunkStartOff + fanoutPiecesPerChunk*crypto.HashSize
		if pieceRootOff < uint32(chunkStartOff) {
			return nil, fmt.Errorf("pieceRootOff < chunkStartOff: %v %v", pieceRootOff, chunkStartOff)
		}
		if pieceRootOff >= uint32(chunkEndOff) {
			return nil, fmt.Errorf("pieceRootOff >= chunkEndOff: %v %v", pieceRootOff, chunkEndOff)
		}

		pieceIndex := (uint64(pieceRootOff) - chunkStartOff) / crypto.HashSize

		// Read the piece root itself.
		_, logicalPieceRootLen := renter.GetPieceOffsetAndLen(skymodules.NewPassthroughErasureCoder(), uint64(pieceRootOff), crypto.HashSize)
		logicalPieceRoot := make([]byte, logicalPieceRootLen)
		_, err = io.ReadFull(td.staticResp, logicalPieceRoot)
		if err != nil {
			return nil, err
		}

		// Read the proof for the piece's root within the fanout.
		pieceRootProofStartOff, pieceRootProofEndOff := segmentAlignOffsets(uint64(pieceRootOff), crypto.HashSize)
		pieceRootProofStart := pieceRootProofStartOff / crypto.SegmentSize
		pieceRootProofEnd := pieceRootProofEndOff / crypto.SegmentSize
		pieceRootProofLen := rhp.RangeProofSize(modules.SectorSize/crypto.SegmentSize, uint64(pieceRootProofStart), uint64(pieceRootProofEnd))

		var pieceRootProof []crypto.Hash
		for i := 0; i < int(pieceRootProofLen); i++ {
			var h crypto.Hash
			_, err = io.ReadFull(td.staticResp, h[:])
			if err != nil {
				return nil, err
			}
			pieceRootProof = append(pieceRootProof, h)
		}

		// Verify the proof.
		valid := crypto.VerifyRangeProof(logicalPieceRoot, pieceRootProof, int(pieceRootProofStart), int(pieceRootProofEnd), td.staticSkylinkRoot)
		if !valid {
			return nil, errors.New("piece root - proof is invalid")
		}

		// Trim the logical piece root to get the actual piece root.
		var pieceRoot crypto.Hash
		copy(pieceRoot[:], logicalPieceRoot[pieceRootOff%crypto.SegmentSize:][:crypto.HashSize])

		// Read the actual piece data next.
		pieceOff, pieceLen := renter.GetPieceOffsetAndLen(fanoutEC, offsetInChunk, length)

		piece := make([]byte, pieceLen)
		_, err = io.ReadFull(td.staticResp, piece)
		if err != nil {
			return nil, err
		}

		// Get the size for the piece proof we are expecting.
		pieceProofStart := pieceOff / crypto.SegmentSize
		pieceProofEnd := (pieceOff + pieceLen) / crypto.SegmentSize
		pieceProofLen := rhp.RangeProofSize(modules.SectorSize/crypto.SegmentSize, pieceProofStart, pieceProofEnd)

		// Download it.
		var pieceProof []crypto.Hash
		for i := 0; i < int(pieceProofLen); i++ {
			var h crypto.Hash
			_, err = io.ReadFull(td.staticResp, h[:])
			if err != nil {
				return nil, err
			}
			pieceProof = append(pieceProof, h)
		}

		// Verify the proof.
		valid = crypto.VerifyRangeProof(piece, pieceProof, int(pieceProofStart), int(pieceProofEnd), pieceRoot)
		if !valid {
			return nil, fmt.Errorf("proof for piece with index %v is invalid (chunkIndex %v sectionIndex %v)", pieceIndex, chunkIndex, currentSection)
		}

		// Set piece in pieces.
		pieces[pieceIndex] = piece
	}

	// Recover the data.
	skipLength := offsetInChunk % (crypto.SegmentSize * uint64(fanoutEC.MinPieces()))
	recoveredBytes := length + skipLength
	buf := bytes.NewBuffer(make([]byte, 0, recoveredBytes))
	err = fanoutEC.Recover(pieces, recoveredBytes, buf)
	if err != nil {
		return nil, err
	}
	return buf.Bytes()[skipLength : skipLength+length], nil
}

// readSmallFile handles the read for small files. Since the small file download
// happens all at once when creating the trustless download, this only copies
// data from an in-memory buffer.
func (td *TrustlessDownload) readSmallFile(b []byte) (int, error) {
	n := copy(b, td.remainingPayload)
	td.remainingPayload = td.remainingPayload[n:]
	if n == 0 {
		return 0, io.EOF
	}
	return n, nil
}

// readSmallFilePayload handles the read for small files. This happens once upon
// creation of the trustless download and the result will be stored in memory.
// Subsequent reads will be served from memoy then.
func (td *TrustlessDownload) readSmallFilePayload() ([]byte, error) {
	// Get the segment-aligned start and end offsets of the data within the
	// sector.
	payloadOff := td.staticLayoutOff + skymodules.SkyfileLayoutSize + td.staticLayout.MetadataSize
	startOff, endOff := segmentAlignOffsets(payloadOff+td.staticFetchOff, td.staticFetchLen)
	proofStart := startOff / crypto.SegmentSize
	proofEnd := endOff / crypto.SegmentSize

	// Read logical payload data.
	baseSectorData := make([]byte, endOff-startOff)
	_, err := io.ReadFull(td.staticResp, baseSectorData)
	if err != nil {
		return nil, err
	}

	// Read the proof.
	nHashes := rhp.RangeProofSize(modules.SectorSize/crypto.SegmentSize, proofStart, proofEnd)
	var proof []crypto.Hash
	for i := uint64(0); i < nHashes; i++ {
		var h crypto.Hash
		_, err = io.ReadFull(td.staticResp, h[:])
		if err != nil {
			return nil, err
		}
		proof = append(proof, h)
	}

	// Verify the proof.
	proofValid := crypto.VerifyRangeProof(baseSectorData, proof, int(proofStart), int(proofEnd), td.staticSkylinkRoot)
	if !proofValid {
		return nil, errors.New("the proof provided for the baseSector wasn't valid")
	}

	// Erasure code the data. We know that it's a 1:n encoding so we use a
	// passthrough encoder.
	offsetInChunk := payloadOff + td.staticFetchOff
	skipLength := offsetInChunk % (crypto.SegmentSize)
	return baseSectorData[skipLength : skipLength+td.staticFetchLen], nil
}

// readExtendedLargeFile handles reading the response of the download of a
// trustless large file with recursive fanout. This works similar to
// readLargeFile but we do receive an additional proof for each level of
// recursion.
// NOTE: This is not required for the kernel right now so support will be
// implemented in the future since the implementation is more involved than
// regular small and large trustless downloads.
func (td *TrustlessDownload) readExtendedLargeFile([]byte) (int, error) {
	return 0, errors.New("trustlessly downloading files with recursive fanouts is not yet supported")
}

// newTrustlessDownload creates a new trustless download that is ready to be
// read from. This will also read and verify the download metadata, layout and
// layout proof already.
func newTrustlessDownload(resp io.ReadCloser, skylink skymodules.Skylink, offset, length uint64) (*TrustlessDownload, error) {
	layoutOff, _, err := skylink.OffsetAndFetchSize()
	if err != nil {
		return nil, err
	}
	td := &TrustlessDownload{
		currentFetchOff:   offset,
		staticFetchOff:    offset,
		staticFetchLen:    length,
		staticLayoutOff:   layoutOff,
		staticResp:        resp,
		staticSkylinkRoot: skylink.MerkleRoot(),
	}
	// Read the metadata.
	if err := td.assertMetadata(); err != nil {
		if err == io.EOF {
			return nil, io.ErrUnexpectedEOF
		}
		return nil, err
	}

	// Read the layout.
	layout, err := td.readLayout()
	if err != nil {
		if err == io.EOF {
			return nil, io.ErrUnexpectedEOF
		}
		return nil, err
	}
	td.staticLayout = layout

	// For small files we read the payload right away too. That's because
	// skyd will download the whole base sector in a single roundtrip
	// anyway. So by the time the layout is available the small file data is
	// too.
	if td.staticLayout.IsSmallFile() {
		td.remainingPayload, err = td.readSmallFilePayload()
		if err != nil {
			return nil, err
		}
	}
	return td, nil
}

// segmentAlignedSize takes a size and padds it to be segment-aligned.
func segmentAlignedSize(n int) int {
	mod := n % crypto.SegmentSize
	if mod != 0 {
		return n + (crypto.SegmentSize - mod)
	}
	return n
}

// segmentAlignedOffsets takes an offset and a length and returns two
// segment-aligned offsets. This can be used for determining the start and end
// of a merkle proof for example.
func segmentAlignOffsets(off, length uint64) (uint64, uint64) {
	offAligned := off
	if mod := offAligned % crypto.SegmentSize; mod != 0 {
		offAligned -= mod
	}
	endOffAligned := off + length
	if mod := endOffAligned % crypto.SegmentSize; mod != 0 {
		endOffAligned += (crypto.SegmentSize - mod)
	}
	return offAligned, endOffAligned
}
