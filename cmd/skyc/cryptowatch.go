package main

// This code pulls SC data from crypto watch for historical pricing information.
//
// There is a go SDK https://github.com/cryptowatch/cw-sdk-go but the last
// version release was 2019 and the one method we would use doesn't support the
// necessary arguments. So much of the code is copied from the SDK here to add
// the functionality we need.
//
// ref: https://docs.cryptowat.ch/

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"gitlab.com/NebulousLabs/errors"
)

type (
	// ohlcResponse is the data type for the response body of the OHLC
	// request. This is copied from the cryptowatch go sdk
	ohlcResponse struct {
		Result map[string][][]json.Number `json:"result"`
	}

	// SCPrice defines the response data for an OHLC interval
	SCPrice struct {
		// High and Low Price for the interval
		Average float64
		High    float64
		Low     float64

		// EndTime is the end of the interval.
		EndTime time.Time

		// StartTime is the beginning of the interval.
		StartTime time.Time
	}
)

// scUSDPrices returns the SCUSD price information
func scUSDPrices(start, end int64) ([]SCPrice, error) {
	return scPrices(start, end, "scusd")
}

// scPrices pulls the historical SC Price information
//
// ref: https://docs.cryptowat.ch/rest-api/markets/ohlc
func scPrices(start, end int64, pair string) (priceInfo []SCPrice, err error) {
	client := &http.Client{}
	var period int64 = 86400 // one day in seconds
	query := fmt.Sprintf("https://api.cryptowat.ch/markets/kraken/%v/ohlc?periods=%v&before=%v&after=%v", pair, period, end, start)
	req, err := http.NewRequest("GET", query, nil)
	if err != nil {
		return nil, errors.AddContext(err, "unable to build request")
	}
	resp, err := client.Do(req)
	if err != nil {
		return nil, errors.AddContext(err, "unable to fetch historical price info")
	}
	defer func() {
		err = errors.Compose(err, resp.Body.Close())
	}()

	srv := ohlcResponse{}

	dec := json.NewDecoder(resp.Body)
	if err := dec.Decode(&srv); err != nil {
		return nil, err
	}

	// Grab the period info
	periodResults, ok := srv.Result[fmt.Sprint(period)]
	if !ok {
		return nil, errors.New("period not found in map")
	}

	priceInfo = make([]SCPrice, 0, len(periodResults))
	for _, pr := range periodResults {
		if len(pr) < 7 {
			return nil, fmt.Errorf("unexpected response from the server: wanted 7 elements, got %v", pr)
		}

		ts, err := pr[0].Int64()
		if err != nil {
			return nil, errors.AddContext(err, fmt.Sprintf("getting timestamp %q", pr[0].String()))
		}
		high, err := pr[2].Float64()
		if err != nil {
			return nil, err
		}
		low, err := pr[3].Float64()
		if err != nil {
			return nil, err
		}
		priceInfo = append(priceInfo, SCPrice{
			Average: (high + low) / 2,
			High:    high,
			Low:     low,

			EndTime:   time.Unix(ts, 0),
			StartTime: time.Unix(ts-period, 0),
		})
	}
	return priceInfo, nil
}
