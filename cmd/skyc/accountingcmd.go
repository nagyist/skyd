package main

import (
	"encoding/csv"
	"fmt"
	"io"
	"math"
	"math/big"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/spf13/cobra"
	"gitlab.com/NebulousLabs/errors"
	"gitlab.com/SkynetLabs/skyd/node/api/client"
	"gitlab.com/SkynetLabs/skyd/skymodules"
	"go.sia.tech/siad/types"
)

var (
	// accountingHeaders are the headers for the csv file generated for the
	// accounting information
	accountingHeaders = []string{"TimeStamp", "Siacoin Balance", "Siafund Balance", "Unspent Unallocated", "Withheld Funds"}

	// transactionHeaders are the headers for the csv file generated for the
	// transaction information
	transactionHeaders = []string{"Date", "Transaction Amount (SC)", "Reason", "Price Average (USD/SC)", "Transaction Amount (USD)"}

	// The following are file names for the various accounting commands
	accountingCSV   = "accounting.csv"
	transactionsCSV = "transactions.csv"
)

var (
	accountingCmd = &cobra.Command{
		Use:   "accounting",
		Short: "Generate a csv file of the accounting information for the node.",
		Long: `Generate a csv file of the accounting information for the node.
Information will be written to accounting.csv`,
		Run: wrap(accountingcmd),
	}

	accountingTxnsCmd = &cobra.Command{
		Use:   "txns",
		Short: "Generate a csv file of the wallet's transactions for the node.",
		Long: `Generate a csv file of the wallet's transactions for the node.
The default is to pull the year to date information. You can use the available flags to define
some preset time periods you would like the information from. See --help
It will include daily pricing information pulled from coin market cap.
Information will be written to transactions.csv

NOTE: Due to the nature of block times being ~10min, there could be small gaps of time that are missed
in the final minutes of the last day of the reported period.`,
		Run: wrap(accountingtxnscmd),
	}
)

// accountingcmd is the handler for the command `skyc accounting`.
// It generates a csv file of the accounting information for the node.
func accountingcmd() {
	// Create csv file
	f, err := os.Create(accountingCSV)
	if err != nil {
		die(err)
	}
	defer func() {
		err = f.Close()
		if err != nil {
			die(err)
		}
	}()

	// Grab the accounting information
	ais, err := httpClient.AccountingGet(accountingRangeStartTime, accountingRangeEndTime)
	if err != nil {
		die("Unable to get accounting information: ", err)
	}

	// Write the information to the csv file.
	err = writeAccountingCSV(ais, f)
	if err != nil {
		die("Unable to write accounting information to csv file: ", err)
	}

	fmt.Println("CSV Successfully generated!")
}

// accountingtxnscmd is the handler for the command `skyc accounting txns`.
// It generates a csv file of the wallet's transactions for the node including
// daily pricing information from coin market cap.
func accountingtxnscmd() {
	// Validate only one option is used
	if (accountingYear != 0 && accountingQuarter != 0) ||
		(accountingYear != 0 && accountingMonth != 0) ||
		(accountingQuarter != 0 && accountingMonth != 0) {
		die("only one flag can be set, year, quarter, or month")
	}

	// Create csv file
	f, err := os.Create(transactionsCSV)
	if err != nil {
		die(err)
	}
	defer func() {
		err = f.Close()
		if err != nil {
			die(err)
		}
	}()

	// Load current time
	today := time.Now()

	// Grab the current block height
	cg, err := httpClient.ConsensusGet()
	if err != nil {
		die(err)
	}

	// Grab the startBlock, endBlock, startTime, and endTime from the year
	var startBlock, endBlock types.BlockHeight
	var startTime, endTime int64
	if accountingYear != 0 {
		fmt.Println("Generating Accounting for", accountingYear)
		currentYear := today.Year()
		if accountingYear > currentYear {
			die("invalid year, can't be in the future", accountingYear)
		}
		yearsInThePast := accountingYear - currentYear
		today = today.AddDate(yearsInThePast, 0, 0)
		startBlock, endBlock, startTime, endTime = startAndEndBlocksAndTimes(cg.Height, today, time.January, time.December)
	}
	if accountingQuarter != 0 {
		fmt.Printf("Generating Accounting for Q%v\n", accountingQuarter)
		var startMonth, endMonth time.Month
		switch accountingQuarter {
		case 1:
			startMonth = time.January
			endMonth = time.March
		case 2:
			startMonth = time.April
			endMonth = time.June
		case 3:
			startMonth = time.July
			endMonth = time.September
		case 4:
			startMonth = time.October
			endMonth = time.December
		default:
			die("invalid quarter, should be 1 - 4")
		}
		startBlock, endBlock, startTime, endTime = startAndEndBlocksAndTimes(cg.Height, today, startMonth, endMonth)
	}
	if accountingMonth != 0 {
		if accountingMonth < 0 || accountingMonth > 12 {
			die("invalid month, should be 1 - 12")
		}
		fmt.Println("Generating Accounting for", time.Month(accountingMonth).String())
		startBlock, endBlock, startTime, endTime = startAndEndBlocksAndTimes(cg.Height, today, time.Month(accountingMonth), time.Month(accountingMonth))
	}
	if accountingYear == 0 && accountingQuarter == 0 && accountingMonth == 0 {
		fmt.Println("Generating Accounting for YTD")
		// Default case, year to date
		//
		// End is the current block height
		endBlock = cg.Height
		endTime = today.Unix()

		// Set start height
		startBlock, startTime = blockAndTime(today, time.January, endBlock, true)
	}

	// Get Historical SCUSD Price Data
	scPriceInfo, err := scUSDPrices(startTime, endTime)
	if err != nil {
		die("Could not get SC Price info: ", err)
	}

	// Get full wallet records
	records, err := accountingWalletTxnRecords(scPriceInfo, startBlock, endBlock, startTime)
	if err != nil {
		die(err)
	}

	// Write the information to the csv file.
	err = writeCSV(transactionHeaders, records, f)
	if err != nil {
		die("Unable to write transaction information to csv file: ", err)
	}

	fmt.Println("CSV Successfully generated!")
}

// accountingWalletTxnRecords generates the wallet txns records to be written to
// a CSV file.
func accountingWalletTxnRecords(scPrices []SCPrice, start, end types.BlockHeight, startTime int64) ([][]string, error) {
	vts, err := walletValuedTransactions(start, end)
	if err != nil {
		return nil, errors.AddContext(err, "could not compute valued transaction")
	}

	// Get full wallet records
	fullRecords := walletTransactionsRecords(vts, startTime)

	// Parse out updated SC records
	var records [][]string
	for _, fullRecord := range fullRecords {
		// Pull out TimeStamp
		timestampStr := fullRecord[0]
		if timestampStr == "unconfirmed" {
			continue
		}
		timestamp, err := time.Parse(walletTxnTimestampFormat, timestampStr)
		if err != nil {
			return nil, err
		}

		// Pull out SC
		scStr := fullRecord[3]

		// Check for negative currency
		isNegativeValue := strings.HasPrefix(scStr, "-")
		hastings, err := types.ParseCurrency(strings.TrimPrefix(scStr, "-"))
		if err != nil {
			return nil, err
		}
		var funds types.Currency
		_, err = fmt.Sscan(hastings, &funds)
		if err != nil {
			return nil, err
		}

		// Convert to USD Price
		scAveragePrice, usdAmount := averageSCPriceAndTxnValue(scPrices, funds, timestamp)

		// Adjust for negative values
		if isNegativeValue {
			usdAmount *= -1
		}
		records = append(records, []string{timestampStr, scStr, "", scAveragePrice, fmt.Sprintf("%.2f", usdAmount)})
	}

	return records, nil
}

// averageSCPriceAndTxnValue pulls out the average SC Price for the timestamp
// and returns it, and converts the hastings to a currency value (ie $10.00)
// based on the price and returns it.
func averageSCPriceAndTxnValue(scPrices []SCPrice, hastings types.Currency, timestamp time.Time) (string, float64) {
	// If hastings is zero, then the conversion is 0
	if hastings.IsZero() {
		return "0", 0
	}
	for _, scp := range scPrices {
		if timestamp.Before(scp.StartTime) || timestamp.After(scp.EndTime) {
			continue
		}
		// Convert hastings to a BigRat
		hastingsRat := new(big.Rat).SetInt(hastings.Big())
		siacoinPrecisionRat := new(big.Rat).SetInt(types.SiacoinPrecision.Big())
		siacoinRat := new(big.Rat).Quo(hastingsRat, siacoinPrecisionRat)

		// Convert to currency
		scpAverageRat := new(big.Rat).SetFloat64(scp.Average)
		avgPriceRat := new(big.Rat).Mul(siacoinRat, scpAverageRat)
		avgPrice, exact := avgPriceRat.Float64()
		if !exact && math.IsInf(avgPrice, 0) {
			die("error getting average price, infinite float")
		}
		return fmt.Sprint(scp.Average), avgPrice
	}
	return "", 0
}

// startAndEndBlocksAndTimes returns the start time, end time, start block, and
// end block based on the start and end month.
func startAndEndBlocksAndTimes(currentHeight types.BlockHeight, today time.Time, startMonth, endMonth time.Month) (startBlock, endBlock types.BlockHeight, startTime, endTime int64) {
	// Determine the start of the month
	startBlock, startTime = blockAndTime(today, startMonth, currentHeight, true)

	// Determine the end of the month
	endBlock, endTime = blockAndTime(today, endMonth, currentHeight, false)
	return
}

// blockAndTime returns the blockHeight and the unix time for either the start
// or end of a given month.
func blockAndTime(today time.Time, month time.Month, refHeight types.BlockHeight, start bool) (types.BlockHeight, int64) {
	blockHeight, dateTime64 := blockAndTimeEstimate(today, month, refHeight, start)

	return blockAndTimeExact(httpClient, blockHeight, dateTime64, start)
}

// blockAndTimeExact returns the exact blockHeight and the unix time for either
// the start or end of a given month.
func blockAndTimeExact(c client.Client, blockHeight types.BlockHeight, dateTime64 int64, start bool) (types.BlockHeight, int64) {
	// Get the unix timestamp
	dateTime := types.Timestamp(dateTime64)

	// Check initial block height to see if it is actually in the
	// range
	cbg, err := c.ConsensusBlocksHeightGet(blockHeight)
	if err != nil {
		die(err)
	}

	// Define helpers
	prev := blockHeight
	next := blockHeight

	if cbg.Timestamp < dateTime {
		// If the timestamp of the block is further in the past than the
		// dateTime, we need to increment the block height until we find
		// the desired block height.
		//
		// For start times, this means finding the lowest block height
		// that is greater than or equal to the dateTime.
		//
		// For end times, this means find the highest block height that
		// is less than or equal to the dateTime.
		for cbg.Timestamp < dateTime {
			prev = next
			next = next + 1
			cbg, err = c.ConsensusBlocksHeightGet(next)
			if err != nil {
				die(err)
			}
		}
		if start || (!start && cbg.Timestamp == dateTime) {
			// For start times we return the next block since its
			// timestamp is >= to the dateTime.
			//
			// We also return the next block for end times if the
			// current timestamp is equal to the dateTime.
			blockHeight = next
		} else {
			// If the timestamps are not equal, we return the prev
			// block for the end times.
			blockHeight = prev
		}
	} else {
		// If the timestamp of the block is further in the future than
		// the dateTime, we need to decrement the block height until we
		// find the desired block height.
		//
		// For start times, this means finding the lowest block height
		// that is greater than or equal to the dateTime.
		//
		// For end times, this means find the highest block height that
		// is less than or equal to the dateTime.
		for cbg.Timestamp > dateTime {
			prev = next
			next = next - 1
			cbg, err = c.ConsensusBlocksHeightGet(next)
			if err != nil {
				die(err)
			}
		}
		if !start || (start && cbg.Timestamp == dateTime) {
			// For end times we return the next block since its
			// timestamp is <= to the dateTime.
			//
			// We also return the next block for start times if the
			// current timestamp is equal to the dateTime.
			blockHeight = next
		} else {
			// If the timestamps are not equal, we return the prev
			// block for the start times.
			blockHeight = prev
		}
	}

	return blockHeight, dateTime64
}

// blockAndTimeEstimate returns the estimated blockHeight and the unix time for
// either the start or end of a given month. The unix time is accurate but the
// block height is an estimated calculation.
func blockAndTimeEstimate(today time.Time, month time.Month, refHeight types.BlockHeight, start bool) (types.BlockHeight, int64) {
	// Grab the year
	year := today.Year()

	// Determine either the first of the month or last of the month
	var date time.Time
	if start {
		date = time.Date(year, month, 1, 0, 0, 0, 0, today.Location())
	} else {
		day := skymodules.DaysInMonth(month, year)
		date = time.Date(year, month, day, 23, 59, 59, 0, today.Location())
	}

	// Calculate an initial estimate of the block height
	secondsSinceDate := uint64(today.Sub(date).Seconds())
	blocksSinceStart := types.BlockHeight(secondsSinceDate) / types.BlockFrequency
	blockHeight := refHeight - blocksSinceStart

	return blockHeight, date.Unix()
}

// writeAccountingCSV is a helper to write the accounting information to a csv
// file.
func writeAccountingCSV(ais []skymodules.AccountingInfo, w io.Writer) error {
	// Build the records. Write Wallet info first, then Renter info.
	var records [][]string
	for _, ai := range ais {
		timeStr := strconv.FormatInt(ai.Timestamp, 10)
		scStr := ai.Wallet.ConfirmedSiacoinBalance.String()
		sfStr := ai.Wallet.ConfirmedSiafundBalance.String()
		usStr := ai.Renter.UnspentUnallocated.String()
		whStr := ai.Renter.WithheldFunds.String()
		record := []string{timeStr, scStr, sfStr, usStr, whStr}
		records = append(records, record)
	}
	return writeCSV(accountingHeaders, records, w)
}

// writeCSV writes the headers and records to a csv file
func writeCSV(headers []string, records [][]string, w io.Writer) error {
	// Create csv writer
	csvW := csv.NewWriter(w)

	// Convert to csv format
	//
	// Write Headers for Reference
	err := csvW.Write(headers)
	if err != nil {
		return errors.AddContext(err, "unable to write header")
	}

	// Write output to file
	err = csvW.WriteAll(records)
	if err != nil {
		return errors.AddContext(err, "unable to write records")
	}

	// Flush the writer and check for an error
	csvW.Flush()
	if err := csvW.Error(); err != nil {
		die("Error when flushing csv writer: ", err)
	}
	return nil
}
